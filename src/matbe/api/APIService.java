package matbe.api;

import java.util.ArrayList;
import java.util.List;
import com.oah.model.*;

import matbe.db;
import matbe.object.DiopObject;


public class APIService {
	
	public APIService() {
	}
	
	public List<DiopStudent> findAll() {
		//username=root, pass=331998
		List<DiopStudent> data = new ArrayList<DiopStudent>();
		ConnectionPool cp = new ConnectionPoolImpl(db.DRIVER, db.URL, db.USER, db.PASSWORD);
		
		List<DiopObject> list = new ArrayList<>();
		Model m = new Model(cp,db.DRIVER, db.URL, db.USER, db.PASSWORD);
		list = m.getAll(DiopObject.class, "LEFT JOIN tblstudent ON diop_student_id = student_id");
		 
		for(int i = 0; i < list.size(); i++){
			DiopStudent DiO = new DiopStudent();
			
			DiO.setId(list.get(i).getDiop_id());
			DiO.setName_student(Utilities.decode(list.get(i).getStudent_name()));
			DiO.setSex(Utilities.decode(list.get(i).getStudent_ismale()));
			DiO.setBirthday(list.get(i).getStudent_birthday());
			DiO.setAddress(Utilities.decode(list.get(i).getStudent_address()));
			DiO.setDiop_inputdate(list.get(i).getDiop_inputdate());
			DiO.setDiop_eye_left(list.get(i).getDiop_eye_left());
			DiO.setDiop_eye_right(list.get(i).getDiop_eye_right());
			DiO.setDiop_astigmatism_left(list.get(i).getDiop_astigmatism_left());
			DiO.setDiop_astigmatism_right(list.get(i).getDiop_astigmatism_right());
			DiO.setDiop_status(Utilities.decode(list.get(i).getDiop_status()));
			
			data.add(DiO);
		}
		return data;
	}
	
	public List<DiopStudent> findBySchoolId(int id) {
		List<DiopStudent> data = new ArrayList<DiopStudent>();
		//username=root, pass=331998
		ConnectionPool cp = new ConnectionPoolImpl(db.DRIVER, db.URL, db.USER, db.PASSWORD);
		
		List<DiopObject> list = new ArrayList<>();
		Model m = new Model(cp,db.DRIVER, db.URL, db.USER, db.PASSWORD);
		list = m.getAll(DiopObject.class, "LEFT JOIN tblstudent ON diop_student_id = student_id "
				+ "LEFT JOIN tblclass ON student_class_id = class_id "
				+ "WHERE class_user_id = "+id);
		 
		for(int i = 0; i < list.size(); i++){
			DiopStudent DiO = new DiopStudent();
			
			DiO.setId(list.get(i).getDiop_id());
			DiO.setName_student(Utilities.decode(list.get(i).getStudent_name()));
			DiO.setSex(Utilities.decode(list.get(i).getStudent_ismale()));
			DiO.setBirthday(list.get(i).getStudent_birthday());
			DiO.setAddress(Utilities.decode(list.get(i).getStudent_address()));
			DiO.setDiop_inputdate(list.get(i).getDiop_inputdate());
			DiO.setDiop_eye_left(list.get(i).getDiop_eye_left());
			DiO.setDiop_eye_right(list.get(i).getDiop_eye_right());
			DiO.setDiop_astigmatism_left(list.get(i).getDiop_astigmatism_left());
			DiO.setDiop_astigmatism_right(list.get(i).getDiop_astigmatism_right());
			DiO.setDiop_status(Utilities.decode(list.get(i).getDiop_status()));
			
			data.add(DiO);
		}
		return data;
	}
	public List<DiopStudent> findByProvinceWithId(String name, int schoolID) {
		List<DiopStudent> data = new ArrayList<DiopStudent>();
		//username=root, pass=331998
		ConnectionPool cp = new ConnectionPoolImpl(db.DRIVER, db.URL, db.USER, db.PASSWORD);
		
		List<DiopObject> list = new ArrayList<>();
		Model m = new Model(cp,db.DRIVER, db.URL, db.USER, db.PASSWORD);
		list = m.getAll(DiopObject.class, "LEFT JOIN tblstudent ON diop_student_id = student_id "
				+ "LEFT JOIN tblclass ON student_class_id = class_id "
				+ "LEFT JOIN tbluser ON class_user_id = user_id "
				+ "WHERE user_address LIKE "+ "'%"+Utilities.encode(name)+"%' "
				+ " AND user_id = " + schoolID);
		 
		for(int i = 0; i < list.size(); i++){
			DiopStudent DiO = new DiopStudent();
			
			DiO.setId(list.get(i).getDiop_id());
			DiO.setName_student(Utilities.decode(list.get(i).getStudent_name()));
			DiO.setSex(Utilities.decode(list.get(i).getStudent_ismale()));
			DiO.setBirthday(list.get(i).getStudent_birthday());
			DiO.setAddress(Utilities.decode(list.get(i).getStudent_address()));
			DiO.setDiop_inputdate(list.get(i).getDiop_inputdate());
			DiO.setDiop_eye_left(list.get(i).getDiop_eye_left());
			DiO.setDiop_eye_right(list.get(i).getDiop_eye_right());
			DiO.setDiop_astigmatism_left(list.get(i).getDiop_astigmatism_left());
			DiO.setDiop_astigmatism_right(list.get(i).getDiop_astigmatism_right());
			DiO.setDiop_status(Utilities.decode(list.get(i).getDiop_status()));
			
			data.add(DiO);
		}
		return data;
	}
	public List<DiopStudent> findByProvinceName(String name) {
		List<DiopStudent> data = new ArrayList<DiopStudent>();
		//username=root, pass=331998
		ConnectionPool cp = new ConnectionPoolImpl(db.DRIVER, db.URL, db.USER, db.PASSWORD);
		
		List<DiopObject> list = new ArrayList<>();
		Model m = new Model(cp,db.DRIVER, db.URL, db.USER, db.PASSWORD);
		list = m.getAll(DiopObject.class, "LEFT JOIN tblstudent ON diop_student_id = student_id "
				+ "LEFT JOIN tblclass ON student_class_id = class_id "
				+ "LEFT JOIN tbluser ON class_user_id = user_id "
				+ "WHERE user_address LIKE "+ "'%"+Utilities.encode(name)+"%'");
		 
		for(int i = 0; i < list.size(); i++){
			DiopStudent DiO = new DiopStudent();
			
			DiO.setId(list.get(i).getDiop_id());
			DiO.setName_student(Utilities.decode(list.get(i).getStudent_name()));
			DiO.setSex(Utilities.decode(list.get(i).getStudent_ismale()));
			DiO.setBirthday(list.get(i).getStudent_birthday());
			DiO.setAddress(Utilities.decode(list.get(i).getStudent_address()));
			DiO.setDiop_inputdate(list.get(i).getDiop_inputdate());
			DiO.setDiop_eye_left(list.get(i).getDiop_eye_left());
			DiO.setDiop_eye_right(list.get(i).getDiop_eye_right());
			DiO.setDiop_astigmatism_left(list.get(i).getDiop_astigmatism_left());
			DiO.setDiop_astigmatism_right(list.get(i).getDiop_astigmatism_right());
			DiO.setDiop_status(Utilities.decode(list.get(i).getDiop_status()));
			
			data.add(DiO);
		}
		return data;
	}

	public static void main(String[] args) {
		APIService api = new APIService();
		List<DiopStudent> d = api.findByProvinceWithId("Tỉnh Vĩnh Phúc", 67);
		System.out.println(d.size());
	}
}
