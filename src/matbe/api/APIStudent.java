package matbe.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;


@WebServlet("/api/student")
public class APIStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	APIService service;
	ObjectMapper mapper;
	
	public APIStudent() {
		service = new APIService();
		mapper = new ObjectMapper();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		
		String schoolId = "", provinceName = "";
		
		schoolId = request.getParameter("schoolId");
		provinceName = request.getParameter("provinceName");
		if(schoolId != "" && provinceName == "") {
			mapper.writeValue(response.getOutputStream(), service.findBySchoolId(Integer.parseInt(schoolId)));
		} else if (provinceName != "" && schoolId == "") {
			mapper.writeValue(response.getOutputStream(), service.findByProvinceName(provinceName));
		} else if (schoolId == "" && provinceName == "") {
			mapper.writeValue(response.getOutputStream(), service.findAll());
		} else if (schoolId != "" && provinceName != "") {
			mapper.writeValue(response.getOutputStream(), service.findByProvinceWithId(provinceName,Integer.parseInt(schoolId)));
		}
	}
	
//	@Override
//	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		request.setCharacterEncoding("UTF-8");
//		response.setContentType("application/json");
//		DiopObject dto = HttpUtils.of(request.getReader()).toModel(DiopObject.class);
//	
//		mapper.writeValue(response.getOutputStream(), service.update(dto));
//		
//	}

}
