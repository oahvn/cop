package matbe.api;
import javax.servlet.ServletRequest;

import net.htmlparser.jericho.*;
public class Utilities {	
	public static String encode(String strUNI) {
		return CharacterReference.encode(strUNI);
	}
	
	public static String decode(String strHTML) {
		return CharacterReference.decode(strHTML);
	}
	
	public static byte getByteParam(ServletRequest request, String name) {
		byte value = -1;
		
		String strValue = request.getParameter(name);
		
		//Kiểm tra
		if(strValue!=null && !strValue.equalsIgnoreCase("")) {
			value = Byte.parseByte(strValue);
		}
		
		return value;
	}
	
	public static short getShortParam(ServletRequest request, String name) {
		short value = -1;
		
		String strValue = request.getParameter(name);
		
		//Kiểm tra
		if(strValue!=null && !strValue.equalsIgnoreCase("")) {
			value = Short.parseShort(strValue);
		}
		
		return value;
	}
	
	public static int getIntParam(ServletRequest request, String name) {
		int value = -1;
		
		String strValue = request.getParameter(name);
		
		//Kiểm tra
		if(strValue!=null && !strValue.equalsIgnoreCase("")) {
			value = Integer.parseInt(strValue);
		}
		
		return value;
	}
	
	public static long getLongParam(ServletRequest request, String name) {
		long value = -1;
		
		String strValue = request.getParameter(name);
		
		//Kiểm tra
		if(strValue!=null && !strValue.equalsIgnoreCase("")) {
			value = Long.parseLong(strValue);
		}
		
		return value;
	}

	public static float getFloatParam(ServletRequest request, String name) {
		float value = -1;
		
		String strValue = request.getParameter(name);
		
		//Kiểm tra
		if(strValue!=null && !strValue.equalsIgnoreCase("")) {
			value = Float.parseFloat(strValue);
		}
		
		return value;
	}

}
