package matbe.api;

public class DiopStudent {
	private int id;
	private String name_student;
	private String address;
	private String birthday;
	private float diop_eye_left;
	private float diop_eye_right;
	private float diop_astigmatism_left;
	private float diop_astigmatism_right;
	private String diop_inputdate;
	private String diop_status;
	private String sex;
	
	
	
	public int getId() {
		return id;
	}



	public String getName_student() {
		return name_student;
	}



	public String getAddress() {
		return address;
	}



	public String getBirthday() {
		return birthday;
	}



	public float getDiop_eye_left() {
		return diop_eye_left;
	}



	public float getDiop_eye_right() {
		return diop_eye_right;
	}



	public float getDiop_astigmatism_left() {
		return diop_astigmatism_left;
	}



	public float getDiop_astigmatism_right() {
		return diop_astigmatism_right;
	}



	public String getDiop_inputdate() {
		return diop_inputdate;
	}



	public String getDiop_status() {
		return diop_status;
	}



	public String getSex() {
		return sex;
	}



	public void setId(int id) {
		this.id = id;
	}



	public void setName_student(String name_student) {
		this.name_student = name_student;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}



	public void setDiop_eye_left(float diop_eye_left) {
		this.diop_eye_left = diop_eye_left;
	}



	public void setDiop_eye_right(float diop_eye_right) {
		this.diop_eye_right = diop_eye_right;
	}



	public void setDiop_astigmatism_left(float diop_astigmatism_left) {
		this.diop_astigmatism_left = diop_astigmatism_left;
	}



	public void setDiop_astigmatism_right(float diop_astigmatism_right) {
		this.diop_astigmatism_right = diop_astigmatism_right;
	}



	public void setDiop_inputdate(String diop_inputdate) {
		this.diop_inputdate = diop_inputdate;
	}



	public void setDiop_status(String diop_status) {
		this.diop_status = diop_status;
	}



	public void setSex(String sex) {
		this.sex = sex;
	}



	public DiopStudent() {
		// TODO Auto-generated constructor stub
	}
	
}