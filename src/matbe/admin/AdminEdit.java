package matbe.admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.ED;
import com.oah.model.ConnectionPool;

import matbe.object.UserObject;

/**
 * Servlet implementation class UserEdit
 */
@WebServlet("/admin-edit")
public class AdminEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminEdit() {
		super();
		// TODO Auto-generated constructor stub
	}

	private boolean validate(String fullname, String mobilephone, String officephone, String address,
			String permission, String page) {
		if (fullname == null || mobilephone == null || officephone == null || address == null || permission == null || page == null) {
			System.out.println("NULL");
			return false;
		} else {
			if (fullname.trim().equalsIgnoreCase("") || fullname.length() > 40) {
			System.out.println("Sai tên");
				return false;
			}
			else if (mobilephone.length() > 11) {
				System.out.println("Sai sđt");
				return false;
			}
			else if (officephone.length() > 11) {
				System.out.println("sai op");
				return false;
				
			}
				
			else if (address.trim().equalsIgnoreCase("") || address.length() > 200) {
				System.out.println("Sai address");
				return false;
			}
				
			try {
				if(Integer.parseInt(permission) >= 4) return false;
			} catch (NumberFormatException e) {
				return false;
			}
			try {
				Integer.parseInt(page);
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
		int id = Integer.parseInt(request.getParameter("id"));
		String page = request.getParameter("p");
		request.setCharacterEncoding("utf-8");
		String fullname = request.getParameter("fn");
		String mobilephone = request.getParameter("mb");
		String officephone = request.getParameter("op");
		String address = request.getParameter("ad");
		String permission = request.getParameter("per");
		ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
		AdminControl uc = new AdminControl(cp);
		if (cp == null) {
			getServletContext().setAttribute("CP", uc.getCP());
		}
		UserObject item = new UserObject();

		if (validate(fullname, mobilephone, officephone, address, permission,page)) {
			item.setUser_id(id);
			item = uc.getById(item, "");
			item.setUser_fullname(ED.encode(fullname));
			item.setUser_mobilephone(mobilephone);
			item.setUser_officephone(officephone);
			item.setUser_address(ED.encode(address));
			item.setUser_permission(Integer.parseInt(permission));
			if (uc.edit(item)) {
				uc.releaseCon();
				response.sendRedirect("/home/admin/users/?page=" + page);
			} else {
				uc.releaseCon();
				response.sendRedirect("/home/admin/users/?page=" + page+"&err=them-that-bai");
			}
		} else {
			response.sendRedirect("/home/admin/users/?page=" + page+"&err=thong-tin-khong-chinh-xac");
		}
		}catch (Exception e) {
			response.sendRedirect("/home/admin/users/?page=1&error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
