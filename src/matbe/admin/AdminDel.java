package matbe.admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.model.ConnectionPool;

import matbe.object.UserObject;

/**
 * Servlet implementation class UserDel
 */
@WebServlet("/admin-del")
public class AdminDel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminDel() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			int page = Integer.parseInt(request.getParameter("page"));
			ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
			AdminControl uc = new AdminControl(cp);
			if (cp == null) {
				getServletContext().setAttribute("CP", uc.getCP());
			}
			UserObject item = new UserObject();
			item.setUser_id(id);

			if (uc.delete(item)) {
				uc.releaseCon();
				response.sendRedirect("/home/admin/users/?page=" + page);
			}
		} catch (NumberFormatException e) {
			response.sendRedirect("/home/admin/users/?page=1");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
