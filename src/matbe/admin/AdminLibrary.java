package matbe.admin;

import java.util.ArrayList;

import com.oah.ED;

import matbe.object.UserObject;

public class AdminLibrary {
	public static String page(int page, int total) {
		StringBuilder sb = new StringBuilder();
		sb.append("<nav aria-label=\"Page navigation example\">\r\n"
				+ "  <ul class=\"pagination justify-content-center\">");
		if (total >= 5) {
			if (page <= 0) {
				sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"/home/admin/users?page=" + (1)
						+ "\">" + 1 + "</a></li>\n");
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page=" + (2)
						+ "\">" + 2 + "</a></li>\n");
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page=" + (3)
						+ "\">" + 3 + "</a></li>\n");
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page=" + (4)
						+ "\">" + 4 + "</a></li>\n");
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page=" + (5)
						+ "\">" + 5 + "</a></li>\n");
			} else {
				if (page == 1) {
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (++page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (++page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (++page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (++page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\">\r\n"
							+ "      <a class=\"page-link\" href=\"/home/admin/users?page=" + total
							+ "\">Trang cuối</a>\r\n" + "    </li>");
				} else if (page == 2) {
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (page - 1) + "\">" + (page - 1) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (++page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (++page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (++page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\">\r\n"
							+ "      <a class=\"page-link\" href=\"/home/admin/users?page=" + total
							+ "\">Trang cuối</a>\r\n" + "    </li>");
				} else if (page == total) {
					sb.append("<li class=\"page-item\">\r\n"
							+ "      <a class=\"page-link\" href=\"/home/admin/users?page=1\">Trang đầu</a>\r\n"
							+ "    </li>");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 4) + "\">" + (total - 4) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 3) + "\">" + (total - 3) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 2) + "\">" + (total - 2) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 1) + "\">" + (total - 1) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total) + "\">" + (total) + "</a></li>\n");
				} else if (page == total - 2) {
					sb.append("<li class=\"page-item\">\r\n"
							+ "      <a class=\"page-link\" href=\"/home/admin/users?page=1\">Trang đầu</a>\r\n"
							+ "    </li>");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 4) + "\">" + (total - 4) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 3) + "\">" + (total - 3) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 2) + "\">" + (total - 2) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 1) + "\">" + (total - 1) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total) + "\">" + (total) + "</a></li>\n");
				} else if (page == total - 1) {
					sb.append("<li class=\"page-item\">\r\n"
							+ "      <a class=\"page-link\" href=\"/home/admin/users?page=1\">Trang đầu</a>\r\n"
							+ "    </li>");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 4) + "\">" + (total - 4) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 3) + "\">" + (total - 3) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 2) + "\">" + (total - 2) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total - 1) + "\">" + (total - 1) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (total) + "\">" + (total) + "</a></li>\n");
				} else if (page >= 3 && page <= (total - 2)) {
					sb.append("<li class=\"page-item\">\r\n"
							+ "      <a class=\"page-link\" href=\"/home/admin/users?page=1\">Trang đầu</a>\r\n"
							+ "    </li>");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (page - 2) + "\">" + (page - 2) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (page - 1) + "\">" + (page - 1) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (page) + "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (page + 1) + "\">" + (page + 1) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page="
							+ (page + 2) + "\">" + (page + 2) + "</a></li>\n");
					sb.append("<li class=\"page-item\">\r\n"
							+ "      <a class=\"page-link\" href=\"/home/admin/users?page=" + total
							+ "\">Trang cuối</a>\r\n" + "    </li>");
				}
			}
		} else {
			for (int i = 1; i <= total; i++) {
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"/home/admin/users?page=" + (i)
						+ "\">" + i + "</a></li>\n");
			}
		}
		sb.append(" </ul>\r\n" + "</nav>");
		return sb.toString();
	}

	public static String views(ArrayList<UserObject> items, int page, int total, int totalPages) {
		StringBuilder sb = new StringBuilder();
		StringBuilder sbPopups = new StringBuilder();
		StringBuilder js = new StringBuilder();
		sb.append("<table border=1 cellspacing=0 style=\"width: 100%;\">\n");
		sb.append("<thead>\n");
		sb.append("<tr>");
		sb.append("<th>STT</th>");
		sb.append("<th>Tài khoản</th>");
		sb.append("<th>Tên khách hàng</th>");
		sb.append("<th>Số điện thoại</th>");
		sb.append("<th>Địa chỉ</th>");
		sb.append("<th>Quyền</th>");
		sb.append("<th colspan=2></th>\n");
		sb.append("<tr>\n");
		sb.append("</thead>\n");
		sb.append("\t<tbody>\n");
		sbPopups.append("<script language=\"javascript\">\n");
		int i = (page - 1) * total;
		for (UserObject item : items) {
			sb.append("<tr>\n");
			sb.append("\t<td>" + (++i) + "</td>");
			sb.append("\n\t<td>" + item.getUser_name() + "</td>");
			sb.append("\n\t<td>" + ED.decode(item.getUser_fullname()) + "</td>");
			sb.append("\n\t<td>" + item.getUser_mobilephone() + "</td>");
			sb.append("\n\t<td>" + ED.decode(item.getUser_address()) + "</td>");
			sb.append("\n\t<td>" + item.getUser_permission() + "</td>");
			sb.append("\n\t<td><button id=\"btn" + item.getUser_id() + "\">Chi tiết</button></td>");
			sb.append("\n\t<td><a id=\"adel"+item.getUser_id()+"\" href=\"#\">&times;</a></td>");
			sb.append("\n</tr>\n");

			sb.append("<script>"
					+ "document.getElementById(\"adel"+item.getUser_id()+"\").onclick=function(){"
							+ "if(confirm('Xác nhận xóa!')){"
							+ "window.location.href=\"/home/admin-del?id="+item.getUser_id()+"&page="+page+"\";"
							+ "}};"
					+ "</script>");
			// popup
			sb.append("<!--POPUP--><div id=\"userModal" + item.getUser_id() + "\" class=\"modal\">\n");
			sb.append("<div class=\"modal-content\">\n");
			sb.append("<span class=\"close\" id=\"close" + item.getUser_id() + "\">&times;</span>\n");
			// Form edit
			sb.append("<form id=\"form" + item.getUser_id() + "\" method=\"POST\" action=\"/home/admin-edit\">\n");
			sb.append("<input type=\"hidden\" name=\"id\" id=\"id" + item.getUser_id() + "\" value=\""
					+ item.getUser_id() + "\" disabled>\n");
			sb.append("<input type=\"hidden\" id=\"page\" value=\"" + page + "\" disabled>\n");
			sb.append("<div class=\"input-group input-group-sm mb-3\">\n");
			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Tài khoản</span>\n");
			sb.append("</div>\n");
			sb.append("<input type=\"text\" class=\"form-control\" name=\"username\" id=\"username" + item.getUser_id()
					+ "\" value=\"" + item.getUser_name() + "\" aria-describedby=\"inputGroup-sizing-sm\" disabled>\n");
			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Tên người dùng</span>\n");
			sb.append("</div>\n");
			sb.append("<input class=\"form-control\" type=\"text\" name=\"fullname\" id=\"fullname" + item.getUser_id()
					+ "\" value=\"" + ED.decode(item.getUser_fullname())
					+ "\" aria-describedby=\"inputGroup-sizing-sm\">\n");
			sb.append("</div>\n");
			sb.append("<div class=\"input-group input-group-sm mb-3\">\n");
			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Số điện thoại</span>\n");
			sb.append("</div>\n");
			sb.append("<input class=\"form-control\" type=\"text\" name=\"mobilephone\" id=\"mobilephone"
					+ item.getUser_id() + "\" value=\"" + item.getUser_mobilephone()
					+ "\" aria-describedby=\"inputGroup-sizing-sm\">\n");
			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Số máy</span>\n");
			sb.append("</div>\n");
			sb.append("<input class=\"form-control\" type=\"text\" name=\"officephone\" id=\"officephone"
					+ item.getUser_id() + "\" value=\"" + item.getUser_officephone()
					+ "\" aria-describedby=\"inputGroup-sizing-sm\">\n");
			sb.append("</div>\n");

			sb.append("<div class=\"input-group input-group-sm mb-3\">\n");
			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Email</span>\n");
			sb.append("</div>\n");
			sb.append("<input class=\"form-control\" type=\"email\" name=\"email\" value=\"" + item.getUser_email()
					+ "\" disabled aria-describedby=\"inputGroup-sizing-sm\">\n");
			sb.append("</div>\n");
			sb.append("<div class=\"input-group input-group-sm mb-3\">\n");
			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Địa chỉ</span>\n");
			sb.append("</div>\n");
			sb.append("<input class=\"form-control\" type=\"text\" name=\"address\" id=\"address" + item.getUser_id()
					+ "\" value=\"" + ED.decode(item.getUser_address())
					+ "\" aria-describedby=\"inputGroup-sizing-sm\">\n");
			sb.append("</div>");
			sb.append("<div class=\"input-group input-group-sm mb-3\">");
			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Quyền</span>\n");
			sb.append("</div>\n");
			sb.append("<input class=\"form-control\" type=\"number\" name=\"permission\" id=\"permission"
					+ item.getUser_id() + "\" value=\"" + item.getUser_permission()
					+ "\" aria-describedby=\"inputGroup-sizing-sm\">\n");

			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Số lần truy cập</span>\n");
			sb.append("</div>\n");
			sb.append("<input class=\"form-control\" type=\"text\" name=\"logined\" value=\"" + item.getUser_logined()
					+ "\" disabled aria-describedby=\"inputGroup-sizing-sm\">\n");
			sb.append("</div>\n");
			sb.append("<div class=\"input-group input-group-sm mb-3\">\n");
			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Ngày tạo</span>\n");
			sb.append("</div>\n");
			sb.append("<input class=\"form-control\" type=\"text\" name=\"created_date\" value=\""
					+ item.getUser_created_date() + "\" disabled aria-describedby=\"inputGroup-sizing-sm\">\n");
			sb.append("<div class=\"input-group-prepend\">\n");
			sb.append("<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Ngày truy cập cuối</span>\n");
			sb.append("</div>\n");
			sb.append("<input class=\"form-control\" type=\"text\" name=\"last_modified\" value=\""
					+ item.getUser_last_modified() + "\" disabled aria-describedby=\"inputGroup-sizing-sm\">\n");
			sb.append("</div>\n");
			sb.append("<a class=\"float-right btn btn-danger\" href=\"#\" id=\"a" + item.getUser_id() + "\">Xóa</a>\n");
			sb.append("<a class=\"btn float-right btn-success mt-2\" id=\"btnUD" + item.getUser_id()
					+ "\">Cập nhật</a>\n");
			sb.append("</form>\n");
			// script
			sbPopups.append("document.getElementById(\"btnUD" + item.getUser_id() + "\").onclick = function(){\n");
			sbPopups.append("var id=document.getElementById(\"id" + item.getUser_id() + "\").value;\n");
			sbPopups.append("var mobilephone"+item.getUser_id()+"=document.getElementById(\"mobilephone" + item.getUser_id() + "\").value;\n");
			sbPopups.append("var officephone"+item.getUser_id()+"=document.getElementById(\"officephone" + item.getUser_id() + "\").value;\n");
			sbPopups.append("var page = document.getElementById(\"page\").value;\n");
			sbPopups.append("var fullname" + item.getUser_id() + "=document.getElementById(\"fullname"
					+ item.getUser_id() + "\").value;\n");
			sbPopups.append("var address" + item.getUser_id() + "=document.getElementById(\"address" + item.getUser_id()
					+ "\").value;\n");
			sbPopups.append("var permission" + item.getUser_id() + " = document.getElementById(\"permission"
					+ item.getUser_id() + "\").value;\n");
			sbPopups.append("if(fullname" + item.getUser_id() + ".trim()==\"\"){\n");
			sbPopups.append("alert(\"Tên người dùng trống!\");\n");
			sbPopups.append("document.getElementById(\"fullname" + item.getUser_id() + "\").focus();\n");
			sbPopups.append("}else if(address" + item.getUser_id() + ".trim()==\"\"){\n");
			sbPopups.append("alert(\"Địa chỉ trống!\");\n");
			sbPopups.append("document.getElementById(\"address" + item.getUser_id() + "\").focus();\n");
			sbPopups.append("}else if(permission" + item.getUser_id() + ".trim()==\"\" || permission"
					+ item.getUser_id() + " < 0){\n");
			sbPopups.append("alert(\"Quyền thực thi!\");\n");
			sbPopups.append("document.getElementById(\"permission" + item.getUser_id() + "\").focus();\n");
			sbPopups.append("}else{\n");
			sbPopups.append("window.location.href= \"/home/admin-edit?id=\"+id+\"&fn=\"+fullname" + item.getUser_id()
					+ "+\"&p=\"+page+\"&ad=\"+address" + item.getUser_id() + "+\"&per=\"+permission" + item.getUser_id()
					+ "+\"&mb=\"+mobilephone"+item.getUser_id()+"+\"&op=\"+officephone"+item.getUser_id()+";");
			sbPopups.append("}\n");
			sbPopups.append("};\n");
			sbPopups.append("document.getElementById(\"a" + item.getUser_id() + "\").onclick = function(){\n");
			sbPopups.append("if(confirm(\"Xác nhận xóa!\")){\n");
			sbPopups.append("window.location.href = \"/home/admin-del?id="+item.getUser_id()+"&page="+page+"\";\n");
			sbPopups.append("}\n");
			sbPopups.append("};\n");
			// End script

			sb.append("</div>");
			sb.append("</div>");

			// script
			sbPopups.append("var userModal" + item.getUser_id() + " = document.getElementById('userModal"
					+ item.getUser_id() + "');\n");
			sbPopups.append(
					"var btn" + item.getUser_id() + " = document.getElementById('btn" + item.getUser_id() + "');\n");
			sbPopups.append(
					"var span" + item.getUser_id() + " = document.getElementById('close" + item.getUser_id() + "');\n");

			sbPopups.append(" btn" + item.getUser_id() + ".onclick = function(){\n");
			sbPopups.append("userModal" + item.getUser_id() + ".style.display = \"block\";\n");
			sbPopups.append("}\n");

			sbPopups.append("span" + item.getUser_id() + ".onclick = function(){\n");
			sbPopups.append("userModal" + item.getUser_id() + ".style.display = \"none\";\n");
			sbPopups.append("}\n");

			// sbPopups.append("window.onclick = function(event){\n");
			// sbPopups.append("if(event.target == userModal" + item.getUser_id() + "){\n");
			// sbPopups.append("userModal" + item.getUser_id() + ".style.display =
			// \"none\";\n");
			// sbPopups.append("}\n");
			// sbPopups.append("} \n");
		}
		sb.append("</tbody>");
		sbPopups.append("</script>\n" + js.toString());
		sb.append("</table><br/>\n");
		sb.append(sbPopups.toString());
		sb.append(page(page, totalPages));
		return sb.toString();
	}

}
