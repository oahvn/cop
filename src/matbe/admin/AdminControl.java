package matbe.admin;

import java.util.ArrayList;

import com.oah.model.ConnectionPool;
import com.oah.model.Model;

import matbe.db;
import matbe.object.UserObject;

public class AdminControl extends Model {

	public AdminControl(ConnectionPool cp) {
		super(cp, db.DRIVER, db.URL, db.USER, db.PASSWORD);
	}

	public String viewUsers(UserObject similar, int page, int total, String conditions) {
		int all = this.gets(similar, -1, total, conditions).size();
		int totalPages = all/total + 1;
		if(page <= 0) page = 1;
		if(page > totalPages) page = totalPages;
		ArrayList<UserObject> items = this.gets(similar, page, total, conditions);
		return AdminLibrary.views(items,page,total,totalPages);
	}

}
