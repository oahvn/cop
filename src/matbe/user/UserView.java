package matbe.user;

import java.util.ArrayList;

import com.oah.ED;
import com.oah.Table;

import matbe.object.ClassObject;
import matbe.object.StudentObject;

public class UserView {
	public static String page(String href, int page, int total) {
		StringBuilder sb = new StringBuilder();
		sb.append("<input type=\"hidden\" id=\"lastPage\" value=\""+total+"\">\n");
		sb.append("<nav aria-label=\"Page navigation example\">\r\n"
				+ "  <ul class=\"pagination justify-content-center\">");
		if (total >= 5) {
			if (page <= 0) {
				sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (1) + "\">"
						+ 1 + "</a></li>\n");
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (2) + "\">" + 2
						+ "</a></li>\n");
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (3) + "\">" + 3
						+ "</a></li>\n");
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (4) + "\">" + 4
						+ "</a></li>\n");
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (5) + "\">" + 5
						+ "</a></li>\n");
			} else {
				if (page == 1) {
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (++page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (++page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (++page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (++page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\">\r\n" + "      <a class=\"page-link\" href=\"" + href + "&page="
							+ total + "\">Trang cuối</a>\r\n" + "    </li>");
				} else if (page == 2) {
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (page - 1)
							+ "\">" + (page - 1) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (++page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (++page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (++page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\">\r\n" + "      <a class=\"page-link\" href=\"" + href + "&page="
							+ total + "\">Trang cuối</a>\r\n" + "    </li>");
				} else if (page == total) {
					sb.append("<li class=\"page-item\">\r\n" + "      <a class=\"page-link\" href=\"" + href
							+ "&page=1\">Trang đầu</a>\r\n" + "    </li>");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 4)
							+ "\">" + (total - 4) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 3)
							+ "\">" + (total - 3) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 2)
							+ "\">" + (total - 2) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 1)
							+ "\">" + (total - 1) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total)
							+ "\">" + (total) + "</a></li>\n");
				} else if (page == total - 2) {
					sb.append("<li class=\"page-item\">\r\n" + "      <a class=\"page-link\" href=\"" + href
							+ "&page=1\">Trang đầu</a>\r\n" + "    </li>");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 4)
							+ "\">" + (total - 4) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 3)
							+ "\">" + (total - 3) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"" + href + "&page="
							+ (total - 2) + "\">" + (total - 2) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 1)
							+ "\">" + (total - 1) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total)
							+ "\">" + (total) + "</a></li>\n");
				} else if (page == total - 1) {
					sb.append("<li class=\"page-item\">\r\n" + "      <a class=\"page-link\" href=\"" + href
							+ "&page=1\">Trang đầu</a>\r\n" + "    </li>");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 4)
							+ "\">" + (total - 4) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 3)
							+ "\">" + (total - 3) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total - 2)
							+ "\">" + (total - 2) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"" + href + "&page="
							+ (total - 1) + "\">" + (total - 1) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (total)
							+ "\">" + (total) + "</a></li>\n");
				} else if (page >= 3 && page <= (total - 2)) {
					sb.append("<li class=\"page-item\">\r\n" + "      <a class=\"page-link\" href=\"" + href
							+ "&page=1\">Trang đầu</a>\r\n" + "    </li>");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (page - 2)
							+ "\">" + (page - 2) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (page - 1)
							+ "\">" + (page - 1) + "</a></li>\n");
					sb.append("<li class=\"active page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (page)
							+ "\">" + page + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (page + 1)
							+ "\">" + (page + 1) + "</a></li>\n");
					sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (page + 2)
							+ "\">" + (page + 2) + "</a></li>\n");
					sb.append("<li class=\"page-item\">\r\n" + "      <a class=\"page-link\" href=\"" + href + "&page="
							+ total + "\">Trang cuối</a>\r\n" + "    </li>");
				}
			}
		} else {
			for (int i = 1; i <= total; i++) {
				sb.append("<li class=\"page-item\"><a class=\"page-link\"href=\"" + href + "&page=" + (i) + "\">" + i
						+ "</a></li>\n");
			}
		}
		sb.append(" </ul>\r\n" + "</nav>");
		return sb.toString();
	}

	private static String optionTerms(ArrayList<ClassObject> items, int page) {
		String tmp = "<option value=\"/home/user/classes/\">Toàn bộ</option>";
		for (ClassObject i : items) {
			tmp += "<option value=\"/home/user/classes?s=" + i.getClass_start_year() + "&f=" + i.getClass_finish_year()
					+ "&page=" + page + "\">" + i.getClass_start_year() + "-" + i.getClass_finish_year()
					+ "</option>\n";
		}
		return tmp;
	}

	private static String optionYears(ArrayList<ClassObject> years, int page) {
		String tmp = "<option value=\"/home/user/classes/\">Toàn bộ</option>";
		for (ClassObject i : years) {
			tmp += "<option value=\"/home/user/classes?y=" + i.getClass_current_year() + "&page=" + page + "\">"
					+ i.getClass_current_year() + "</option>\n";
		}
		return tmp;
	}

	public static String views(ArrayList<ClassObject> items, int page, int total, int totalPages,
			ArrayList<ClassObject> terms, ArrayList<ClassObject> years) {
		StringBuilder sb = new StringBuilder();
		StringBuilder js = new StringBuilder();
		sb.append("<table border=1 cellspacing=0 style=\"width: 100%;\">\n");

		// -------------HEADER------------------//
		String slTerm = "<select id = \"slTerm\" class=\"form-control form-control-sm\">\r\n"
				+ "<option selected=\"selected\" disabled=\"disabled\">Niên khóa</option>\r\n"
				+ optionTerms(terms, page) + "</select>";
		String slYear = "<select id=\"slYear\" class=\"form-control form-control-sm\">\r\n"
				+ "<option class=\"form-control\" selected=\"selected\" disabled=\"disabled\">Năm học hiện tại</option>\r\n"
				+ optionYears(years, page) + "</select>";
		String header = Table.header("STT", "Tài khoản", "Tên lớp", "Giáo viên CN", slTerm, slYear, "Chi tiết", "DSHS",
				"XÓA");
		// ------------END HEADER---------------//
		sb.append(header);

		sb.append("\t<tbody>\n");
		js.append("<script language=\"javascript\">\n");
		int i = (page - 1) * total;
		String username = "";
		String classname = "";
		String teachername = "";
		String term = "";
		String current = "";
		String btnShow = "";
		String btnStudent = "";
		String adel = "";
		String row = "";
		for (ClassObject item : items) {
			int id = item.getClass_id();
			// -----------------------ROW---------------------//
			username = item.getClass_username();
			classname = ED.decode(item.getClass_name());
			teachername = ED.decode(item.getClass_teacher_name());
			term = item.getClass_start_year() + "-" + item.getClass_finish_year() + "";
			current = item.getClass_current_year() + "";
			btnShow = "<button class=\"btn btn-sm btn-outline-light\" id=\"btnShow" + id
					+ "\">Chi tiết</button>";
			btnStudent = "<a class=\"btn btn-sm btn-outline-light\" href=\"/home/user/classes/students/?id="+id+"\">DSHS</a>";
			adel = "<a class=\"btn btn-sm btn-outline-light\" id=\"adel" + id
					+ "\" href=\"#\"><i class=\"far fa-trash-alt\"></i></a>";
			row = Table.row(++i + "", username, classname, teachername, term, current, btnShow, btnStudent, adel);
			sb.append(row);
			// -----------------------END ROW-----------------//

			// -----------------------SCRIPT FOR DELETE ON ROW----------------------//
			sb.append("<script>" + "document.getElementById(\"adel" + id + "\").onclick=function(){\n"
					+ "if(confirm('Xác nhận xóa!')){" + "window.location.href=\"/home/user-del?id=" + id + "&page="
					+ page + "\";" + "}};\n" + "</script>\n");
			// -----------------------END SCRIPT----------------------------------//

			// -----------------------POPUP FORM FOR EDIT OR DELETE-----------------//
			sb.append("<div id=\"myModal" + id + "\" class=\"modal\">" + "<div class=\"modal-content\">"
					+ "<span id=\"close" + id + "\" class=\"close\">&times;</span>" + "<p>"
					+ "<form id=\"frm" + id + "\" method=\"POST\" action=\"/home/user-edit?page=" + page
					+ "\">" + "<input type=\"hidden\" id=\"id"+id+"\" value=\"" + id + "\">"
					+ "<div class=\"input-group input-group-sm mb-3\">" + "<div class=\"input-group-prepend\">"
					+ "<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Tài khoản</span>" + "</div>"
					+ "<input type=\"text\" class=\"form-control\" name=\"username\" value=\""
					+ item.getClass_username() + "\" disabled aria-describedby=\"inputGroup-sizing-sm\">"
					+ "<div class=\"input-group-prepend\">"
					+ "<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Tên lớp</span>" + "</div>"
					+ "<input class=\"form-control\" type=\"text\" name=\"classname\" id=\"classname"
					+ id + "\" value=\"" + ED.decode(item.getClass_name())
					+ "\" aria-describedby=\"inputGroup-sizing-sm\">" + "</div>" + "<!-- Giáo viên chủ nghiệm -->"
					+"<!-- Mật khẩu -->"
					+"<div class=\"input-group input-group-sm mb-3\">"
					+"<div class=\"input-group-prepend\">"
					+"	<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">mật khẩu</span>"
					+"</div>"
					+"<input class=\"form-control\" type=\"password\" name=\"password\" id=\"password"+id+"\" value=\""+item.getClass_password()+"\" aria-describedby=\"inputGroup-sizing-sm\">"
					+"</div>"
					+ "<div class=\"input-group input-group-sm mb-3\">" + "<div class=\"input-group-prepend\">"
					+ "<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Giáo viên chủ nhiệm</span>"
					+ "</div>" + "<input class=\"form-control\" type=\"text\" name=\"teacher"+id+"\" id=\"teacher"
					+ id + "\" value=\"" + ED.decode(item.getClass_teacher_name())
					+ "\" aria-describedby=\"inputGroup-sizing-sm\">" + "</div>" + "<!-- Địa chỉ -->"
					+ "<div class=\"input-group input-group-sm mb-3\">" + "<div class=\"input-group-prepend\">"
					+ "<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Địa chỉ</span>" + "</div>"
					+ "<input class=\"form-control\" type=\"text\" name=\"address\" id=\"address"+id+"\" value=\""
					+ ED.decode(item.getClass_teacher_address()) + "\" aria-describedby=\"inputGroup-sizing-sm\">"
					+ "</div>" + "<div class=\"input-group input-group-sm mb-3\">"
					+ "<div class=\"input-group-prepend\">"
					+ "<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Số điện thoại</span>" + "</div>"
					+ "<input class=\"form-control\" type=\"text\" name=\"phone\" id=\"phone"+id+"\" value=\""
					+ item.getClass_teacher_phone() + "\" aria-describedby=\"inputGroup-sizing-sm\">"
					+ "<div class=\"input-group-prepend\">"
					+ "<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Năm học hiện tại</span>" + "</div>"
					+ "<input class=\"form-control\" type=\"text\" name=\"current\" id=\"current" + id
					+ "\" value=\"" + item.getClass_current_year() + "\" aria-describedby=\"inputGroup-sizing-sm\">"
					+ "</div>" + "<!-- Khóa học -->" + "<div class=\"input-group input-group-sm mb-3\">"
					+ "<div class=\"input-group-prepend\">"
					+ "<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Niên khóa: Từ</span>" + "</div>"
					+ "<input class=\"form-control\" type=\"text\" name=\"start\" id=\"start" + id
					+ "\" value=\"" + item.getClass_start_year() + "\" aria-describedby=\"inputGroup-sizing-sm\">"
					+ "<div class=\"input-group-prepend\">"
					+ "<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Đến</span>" + "</div>"
					+ "<input class=\"form-control\" type=\"text\" name=\"finish\" id=\"finish" + id
					+ "\" value=\"" + item.getClass_finish_year() + "\" aria-describedby=\"inputGroup-sizing-sm\">"
					+ "</div>" + "<a class=\"float-right btn btn-danger\" href=\"#\" id=\"btnDL" + id
					+ "\">Xóa</a>" + "<a class=\"btn float-right btn-success mt-2\" href=\"#\" id=\"btnUD"
					+ id + "\">Cập nhật</a>" + "</form>" + "</p>" + "</div>" + "</div>");
			// -----------------------------------END POPUP------------------------------//
			
			// -----------------------------------SCRIPT FOR POPUPS
			// FORM-------------------------//
			js.append("document.getElementById(\"btnUD" + id + "\").onclick = function(){"
					+ "\nvar id"+id+"=document.getElementById(\"id"+id+"\");"
					+ "\nvar classname"+id+"=document.getElementById(\"classname"+id+"\");"
					+ "\nvar teacher"+id+"=document.getElementById(\"teacher"+id+"\");"
					+ "\nvar phone"+id+" = document.getElementById(\"phone"+id+"\");"
					+ "\nvar address"+id+" = document.getElementById(\"address"+id+"\");"
					+ "\nvar start"+id+" = document.getElementById(\"start"+id+"\");"
					+ "\nvar finish"+id+" = document.getElementById(\"finish"+id+"\");"
					+ "\nvar current"+id+" = document.getElementById(\"current"+id+"\");"
					+ "\nvar password"+id+" = document.getElementById(\"password"+id+"\");"
					+ "\nvar regVi = /[^a-z0-9A-Z_\\-\\.ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]/;"

					+ "\nif(classname" + id + ".value.trim().match(regVi)){"
					+ "alert(\"Tên lớp chứa kí tự đặc biệt\");" + "classname" + id + ".focus();"
					+ "classname" + id + ".select();" + "\n}else if(classname" + id
					+ ".value.trim().length  == 0){" + "alert(\"Tên lớp trống\");" + "classname" + id
					+ ".focus();" + "classname" + id + ".select();" + "\n}else if(classname"
					+ id + ".value.trim().length  > 100){" + "alert(\"Tên lớp quá dài\");" + "classname"
					+ id + ".focus();" + "classname" + id + ".select();"
					+ "\n}else if(teacher" + id + ".value.trim().match(regVi)){"
					+ "alert(\"Tên giáo viên chứa ký tự đặc biệt\");" + "teacher" + id + ".focus();"
					+ "teacher" + id + ".select();" + "\n}else if(teacher" + id
					+ ".value.trim().length == 0){" + "alert(\"Tên giáo viên quá ngắn\");" + "teacher"
					+ id + ".focus();" + "teacher" + id + ".select();"
					+ "\n}else if(teacher" + id + ".value.trim().length > 30){"
					+ "alert(\"Tên giáo viên quá dài\");" + "teacher" + id + ".focus();" + "teacher"
					+ id + ".select();" + "\n}else if(start" + id
					+ ".value.trim().length == 0){" + "alert(\"Năm bắt đầu trống\");" + "start" + id
					+ ".focus();" + "start" + id + ".select();" + "\n}else if(start"
					+ id + ".value > (new Date().getFullYear()+10)){"
					+ "alert(\"Năm bắt đầu quá lớn\");" + "start" + id + ".focus();" + "start"
					+ id + ".select();" + "\n}else if(finish" + id
					+ ".value.trim().length == 0){" + "alert(\"Năm kết thúc đầu trống\");" + "finish"
					+ id + ".focus();" + "finish" + id + ".select();"
					+ "\n}else if(finish" + id + ".value > (new Date().getFullYear()+10)){"
					+ "alert(\"Năm kết thúc đầu quá lớn\");" + "finish" + id + ".focus();" + "finish"
					+ id + ".select();" + "\n}else if(current" + id + ".value < start"
					+ id + ".value || current" + id + ".value > finish"
					+ id + ".value){" + "alert(\"Sai năm học hiện tại\");" + "current"
					+ id + ".focus();" + "current" + id + ".select();" + "\n}else{"
					+ "window.location.href = \"/home/user-edit?page="+page+"&id=\"+id" + id + ".value+\"&classname=\"+classname" + id
					+ ".value+\"&teacher=\"+teacher" + id + ".value+\"&phone=\"+phone" + id + ".value+\"&address=\"+address" + id
					+ ".value+\"&current=\"+current" + id + ".value+\"&start=\"+start" + id + ".value+\"&finish=\"+finish" + id + ".value"
					+"+\"&password=\"+password"+id+".value;"
					+ "}\n" + "};\n"

					+ "document.getElementById(\"btnDL" + id + "\").onclick = function(){\n"
					+ "\nif(confirm(\"Xác nhận xóa!\")){\n" + "window.location.href = \"/home/user-del?id="+id+"&page="+page+"\";\n"
					+ "}" + "};"

					+ "\nvar myModal" + id + " = document.getElementById('myModal" + id
					+ "');" + "\nvar btnShow" + id + " = document.getElementById('btnShow"
					+ id + "');" + "\nvar close" + id
					+ " = document.getElementById('close" + id + "');"

					+ "\nbtnShow" + id + ".onclick = function(){" + "myModal" + id
					+ ".style.display = \"block\";\n" + "}"

					+ "\nclose" + id + ".onclick = function(){" + "myModal" + id
					+ ".style.display = \"none\";\n" + "}\n"

					+ "window.onclick = function(event){\n" + "\nif(event.target == myModal" + id
					+ "){\n" + "myModal.style.display = \"none\";\n" + "}\n" + "}\n");
			// ---------------------------------END SCRIPT
			// ---------------------------------//
		}
		js.append("</script>\n");
		sb.append("</tbody>");
		sb.append("</table><br/>\n");
		sb.append(js.toString());

		// --------------------------------------page-----------------------------------//
		sb.append(page("/home/user/classes?", page, totalPages));
		return sb.toString();
	}
	
	public static String viewStudents(ArrayList<StudentObject> items, int page, int total, int totalPages) {
		String tmp = "";
		tmp+=("<table border=1 cellspacing=0 style=\"width: 100%;\">\n");
		tmp+=Table.header("STT","Tên học sinh","Giới tính","Ngày sinh","Địa chỉ","Sửa","Xóa");
		tmp+="<tbody>";
		String popups= "";
		String js = "<script>\n";
		int stt = (page-1)*total;
		int class_id = 0;
		if(items!=null && !items.isEmpty()) {
			class_id = items.get(0).getStudent_class_id();
		}
		for(StudentObject i : items) {
			String name = ED.decode(i.getStudent_name());
			String sex = ED.decode(i.getStudent_ismale());
			String birthday = i.getStudent_birthday();
			String address = ED.decode(i.getStudent_address());
			int id = i.getStudent_id();
			String btnEdit = "<button class=\"btn btn-sm btn-outline-light\" id=\"btnEdit"+id+"\">Sửa</button>";
			String btnDel = "<a id=\"btnDel"+id+"\" class=\"btn btn-sm btn-outline-light\" href=\"#\"><i class=\"far fa-trash-alt\"></a>";
			tmp+=Table.row(""+(++stt),name,sex,birthday,address,btnEdit,btnDel);
			String maleChk = "";
			String femaleChk = "";
			if(sex.equalsIgnoreCase("Nam")) maleChk="checked";
			else femaleChk = "checked";
popups+="<div id=\"modal"+id+"\" class=\"modal\">\n"
	+"<div class=\"modal-content\">\n"
		+"<span id=\"close"+id+"\" class=\"close\">&times;</span>\n"
		+"<p>\n"
			+"<form>\n"
				+"<div class=\"input-group input-group-sm mb-3\">\n"
					+"<div class=\"input-group-prepend\">\n"
						+"<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Họ và tên</span>\n"
					+"</div>\n"
					+"<input class=\"form-control\" type=\"text\" id=\"name"+id+"\" value=\""+name+"\" aria-describedby=\"inputGroup-sizing-sm\">\n"
				+"</div>\n"
				+"<div class=\"input-group input-group-sm mb-3\">\n"
					+"<div class=\"input-group-prepend\">\n"
						+"<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Giới tính</span>\n"
					+"</div>\n"
					+"<div class=\"custom-control custom-radio custom-control-inline ml-2\">\n"
						+"<input type=\"radio\" id=\"male"+id+"\" "+maleChk+" name=\"sex\" class=\"custom-control-input\">\n"
						+"<label class=\"custom-control-label\"for=\"male"+id+"\">Nam</label>\n"
					+"</div>\n"
					+"<div class=\"custom-control custom-radio custom-control-inline\">\n"
						+"<input type=\"radio\" id=\"female"+id+"\" "+femaleChk+" name=\"sex\" class=\"custom-control-input\">\n"
						+"<label class=\"custom-control-label\" for=\"female"+id+"\">Nữ</label>\n"
					+"</div>\n"
					+"<div class=\"input-group-prepend\">\n"
						+"<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Ngày sinh</span>\n"
					+"</div>\n"
					+"<input class=\"form-control\" type=\"DATE\" id=\"birthday"+id+"\" value=\""+birthday+"\"\n"
					+"aria-describedby=\"inputGroup-sizing-sm\">\n"

				+"</div>\n"
				+"<div class=\"input-group input-group-sm mb-3\">\n"
					+"<div class=\"input-group-prepend\">\n"
						+"<span class=\"input-group-text\" id=\"inputGroup-sizing-sm\">Địa chỉ</span>\n"
					+"</div>\n"
					+"<input class=\"form-control\" type=\"text\" id=\"address"+id+"\" value=\""+address+"\"\n"
					+"aria-describedby=\"inputGroup-sizing-sm\">	\n"

				+"</div>\n"
				+"<input type=\"hidden\" value=\""+id+"\" id=\"sId"+id+"\">\n"
				+"<a class=\"btn float-right btn-success mt-2\" href=\"#\"\n"
				+"id=\"btnUD"+id+"\">Cập nhật</a>\n"
			+"</form>\n"
		+"</p>\n"
	+"</div>\n"
+"</div>\n";
		js+="document.getElementById(\"btnDel"+id+"\").onclick = function(){\n"
				+ "if(confirm(\"Xác nhận xóa\")){\n"
				+ "\twindow.location.href = \"/home/user-del-student?cId="+class_id+"&page="+page+"&id="+id+"\";\n"
				+ "}\n"
				+ "};\n";
			
		js+="document.getElementById(\"btnUD"+id+"\").onclick = function(){\n"
				+"var name"+id+"=document.getElementById(\"name"+id+"\");\n"
				+"var male"+id+"=document.getElementById(\"male"+id+"\");\n"
				+"var birthday"+id+"=document.getElementById(\"birthday"+id+"\");\n"
				+"var address"+id+"=document.getElementById(\"address"+id+"\");\n"
				+"var	sId"+id+" = document.getElementById(\"sId"+id+"\").value;\n"
				+"var regVi = /[^a-z0-9A-Z_\\-\\.ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]/;\n"
				+"if(name"+id+".value.trim().match(regVi)){\n"
					+"alert(\"Tên chứa kí tự đặc biệt\");\n"
					+"name"+id+".focus();\n"
					+"name"+id+".select();\n"
				+"}else if(name"+id+".value.trim().length  == 0){\n"
					+"alert(\"Tên trống\");\n"
					+"name"+id+".focus();\n"
					+"name"+id+".select();\n"
				+"}else if(name"+id+".value.trim().length  > 30){\n"
					+"alert(\"Tên quá dài\");\n"
					+"name"+id+".focus();\n"
					+"name"+id+".select();\n"
				+"}else if(birthday"+id+".value.trim()==0){\n"
					+"alert(\"Ngày sinh trống\");\n"
					+"birthday"+id+".focus();\n"
					+"birthday"+id+".select();\n"
				+"}else if(address"+id+".value.trim().match(regVi)){\n"
					+"alert(\"Địa chỉ chứ ký tự đặc biệt\");\n"
					+"address"+id+".focus();\n"
					+"address"+id+".select();\n"
				+"}else if(address"+id+".value.trim().length > 100){\n"
					+"alert(\"Địa chỉ quá dài\");\n"
					+"address"+id+".focus();\n"
					+"address"+id+".select();\n"
				+"}else if(address"+id+".value.trim().length == 0){\n"
					+"alert(\"Địa chỉ trống\");\n"
					+"address"+id+".focus();\n"
					+"address"+id+".select();\n"
				+"}else{\n"
					+"var sex = \"Nữ\";\n"
					+"if(male"+id+".checked){\n"
						+"sex = \"Nam\";\n"
					+"}\n"
					+"var id = document.getElementById(\"id\").value;\n"
					+"window.location.href = \"/home/user-edit-student?page="+page+"&name=\"+name"+id+".value+\"&sex=\"+sex+\"&birthday=\"+birthday"+id+".value+\"&address=\"+address"+id+".value+\"&sId=\"+sId"+id+";\n"
				+"}\n"
			+"};\n"
		+"				\n"
			+"var modal"+id+" = document.getElementById(\"modal"+id+"\");\n"
			+"var btnEdit"+id+" = document.getElementById(\"btnEdit"+id+"\");\n"
			+"var close"+id+" = document.getElementById(\"close"+id+"\");\n"
			+"btnEdit"+id+".onclick = function(){\n"
				+"modal"+id+".style.display = \"block\";\n"
			+"}\n"
			+"close"+id+".onclick = function(){\n"
				+"modal"+id+".style.display = \"none\";\n"
			+"}\n"
			+"window.onclick = function(event){\n"
				+"if(event.target == modal"+id+"){\n"
					+"modal"+id+".style.display = \"none\";\n"
				+"}\n"
			+"}\n";
		}
		tmp+=popups;
		js+="</script>\n";
		popups+=js;
		tmp+="</tbody>";
		tmp+="</table>";
		tmp+=popups;
		tmp+=page("/home/user/classes/students?id="+class_id+"", page, totalPages);
		return tmp;
	}
}
