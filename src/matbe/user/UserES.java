package matbe.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.ED;
import com.oah.model.ConnectionPool;

import matbe.object.StudentObject;

/**
 * Servlet implementation class UserES
 */
@WebServlet("/user-edit-student")
public class UserES extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserES() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String name = request.getParameter("name");
		String sex = request.getParameter("sex");
		String birthday = request.getParameter("birthday");
		String address = request.getParameter("address");
		String id = request.getParameter("sId");
		String page = request.getParameter("page");
		int ID = 0;
		if(id!=null) {
			try {
				ID = Integer.parseInt(id);
			}catch (NumberFormatException e) {
			}
			
			ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
			UserControl uc = new UserControl(cp);
			if (cp == null) {
				getServletContext().setAttribute("CP", uc.getCP());
			}
			
			StudentObject item = new StudentObject();
			item.setStudent_id(ID);
			item = uc.getById(item, "");
			item.setStudent_name(ED.encode(name));
			item.setStudent_ismale(ED.encode(sex));
			item.setStudent_birthday(birthday);
			item.setStudent_address(ED.encode(address));
			
			if(uc.edit(item)) {
				uc.releaseCon();
				response.sendRedirect("/home/user/classes/students?id="+item.getStudent_class_id()+"&page="+page);
			}else {
				uc.releaseCon();
				response.sendRedirect("/home/user/classes/students?id="+item.getStudent_class_id()+"&page="+page+"&error");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
