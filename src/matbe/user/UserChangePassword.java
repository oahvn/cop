package matbe.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.model.ConnectionPool;

import matbe.admin.AdminControl;
import matbe.object.UserObject;

/**
 * Servlet implementation class UserChangePassword
 */
@WebServlet("/user-change-password")
public class UserChangePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserChangePassword() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		try {
			String id = request.getParameter("id");
			String password = request.getParameter("newpass");
			int ID = 0;
			if(id!=null) {
				ID = Integer.parseInt(id);
			}
			ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
			AdminControl uc = new AdminControl(cp);
			if (cp == null) {
				getServletContext().setAttribute("CP", uc.getCP());
			}
			UserObject item = new UserObject();
			item.setUser_id(ID);
			item = uc.getById(item, "");
			if(item!=null) {
				item.setUser_pass(password);
				if(uc.edit(item)) {
					uc.releaseCon();
					response.sendRedirect("/home/user/change-password?status=successed");
				}else {
					response.sendRedirect("/home/user/change-password?status=error");
				}
			}else {
				response.sendRedirect("/home/user/change-password?status=error");
			}
		}catch (NumberFormatException e) {
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
