package matbe.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.ED;
import com.oah.model.ConnectionPool;

import matbe.object.UserObject;

/**
 * Servlet implementation class UserSignin
 */
@WebServlet("/user-signin")
public class UserSignin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserSignin() {
		super();
		// TODO Auto-generated constructor stub
	}

	private boolean validate(String username, String password) {
		if (username == null || password == null)
			return false;
		else if (username.trim().equalsIgnoreCase("") || password.trim().equalsIgnoreCase(""))
			return false;
		return true;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
		UserControl uc = new UserControl(cp);
		uc.releaseCon();
		if (cp == null) {
			getServletContext().setAttribute("CP", uc.getCP());
		}
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if (validate(username, password)) {
			username = username.replace("'", "");
			password = password.replace("'", "");

			UserObject UserLogined = uc.getByConditions(UserObject.class,
					"WHERE user_name = '" + username + "' AND user_pass='" + ED.encode(password) + "'");

			uc.releaseCon();
			if (UserLogined != null && UserLogined.getUser_permission() == 2) {
				request.getSession().setAttribute("UserLogined", UserLogined);
				response.sendRedirect("/home/user/home");
			} else {
				response.sendRedirect("/home/user/signin/?err=dang-nhap-that-bai");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
