package matbe.user;

import java.util.ArrayList;

import com.oah.ED;
import com.oah.model.ConnectionPool;
import com.oah.model.Model;

import matbe.db;
import matbe.object.ClassObject;
import matbe.object.StudentObject;

public class UserControl extends Model {

	public UserControl(ConnectionPool cp) {
		super(cp, db.DRIVER, db.URL, db.USER, db.PASSWORD);
	}

	public String viewClasses(ClassObject similar, int page, int total) {
		String join = " LEFT JOIN tbluser ON class_user_id = user_id WHERE user_id = " + similar.getClass_user_id()
				+ " ";
		int all = this.getAll(ClassObject.class, join).size();
		int totalPages = (all % total == 0) ? all / total : (all / total + 1);
		if (page > totalPages)
			page = totalPages;
		ArrayList<ClassObject> items = this.gets(similar, page, total, join);
		ArrayList<ClassObject> terms = this.getAll(ClassObject.class, join + " GROUP BY class_start_year");
		ArrayList<ClassObject> years = this.getAll(ClassObject.class, join + " GROUP BY class_current_year");
		if (similar.getClass_finish_year() != 0 && similar.getClass_start_year() != 0
				&& similar.getClass_current_year() == 0) {
			all = this.getAll(ClassObject.class, join + " AND class_start_year = " + similar.getClass_start_year()
					+ " AND class_finish_year = " + similar.getClass_finish_year()).size();
			items = this.gets(similar, page, total, join + " AND class_start_year = " + similar.getClass_start_year()
					+ " AND class_finish_year = " + similar.getClass_finish_year());
			totalPages = (all % total == 0) ? all / total : (all / total + 1);
		} else if (similar.getClass_finish_year() == 0 && similar.getClass_start_year() == 0
				&& similar.getClass_current_year() != 0) {
			all = this.getAll(ClassObject.class, join + " AND class_current_year = " + similar.getClass_current_year())
					.size();
			items = this.gets(similar, page, total,
					join + " AND class_current_year = " + similar.getClass_current_year());
			totalPages = (all % total == 0) ? all / total : (all / total + 1);
		} else if (similar.getClass_finish_year() == 0 && similar.getClass_start_year() == 0
				&& similar.getClass_current_year() == 0 && similar.getClass_name() != null) {
			all = this.getAll(ClassObject.class,
					join + " AND " + "(class_username LIKE '%" + similar.getClass_name() + "%' OR "
							+ "class_teacher_name LIKE '%" + similar.getClass_name() + "%' OR " + "class_name LIKE '%"
							+ similar.getClass_name() + "%')")
					.size();
			items = this.gets(similar, page, total,
					join + " AND " + "(class_username LIKE '%" + similar.getClass_name() + "%' OR "
							+ "class_teacher_name LIKE '%" + similar.getClass_name() + "%' OR " + "class_name LIKE '%"
							+ similar.getClass_name() + "%')");
			totalPages = (all % total == 0) ? all / total : (all / total + 1);
		}

		return UserView.views(items, page, total, totalPages, terms, years);
	}

	public String viewStudents(StudentObject similar, int page, int total) {
		String sql = "LEFT JOIN tblclass ON student_class_id = class_id WHERE student_class_id = "
				+ similar.getStudent_class_id();
		ArrayList<StudentObject> items = this.gets(similar, page, total, sql);
		int all = this.getAll(StudentObject.class, sql).size();
		int totalPage = (all % total == 0) ? all / total : all / total + 1;
		if(similar.getStudent_name()!=null && !similar.getStudent_name().equalsIgnoreCase("")) {
			String find = " AND ("
							+ "student_name LIKE '%"+ED.encode(similar.getStudent_name())+"%' OR "
							+ "student_birthday LIKE '%"+similar.getStudent_name()+"%' OR "
							+ "student_ismale LIKE '%"+ED.encode(similar.getStudent_name())+"%' OR "
							+ "student_address LIKE '%"+ED.encode(similar.getStudent_name())+"%')";
			items = this.gets(similar, page, total, sql + find);
			all = this.getAll(StudentObject.class, sql+find).size();
			totalPage = (all % total == 0) ? all / total : all / total + 1;
		}
		return UserView.viewStudents(items, page, total, totalPage);
	}

}
