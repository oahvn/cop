package matbe.user;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.model.ConnectionPool;

import matbe.object.UserObject;

/**
 * Servlet implementation class UserForgot
 */
@WebServlet("/user-forgot")
public class UserForgot extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserForgot() {
		super();
		// TODO Auto-generated constructor stub
	}

	private boolean validate(String username, String password) {
		if (username == null || password == null)
			return false;
		else if (username.trim().equalsIgnoreCase("") || password.trim().equalsIgnoreCase(""))
			return false;
		return true;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		UserObject UserForgot = (UserObject) request.getSession().getAttribute("UserForgot");
		String realCode = (String) request.getSession().getAttribute("code");
		String fakeCode = request.getParameter("code");
		ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
		UserControl uc = new UserControl(cp);
		uc.releaseCon();
		if (cp == null) {
			getServletContext().setAttribute("CP", uc.getCP());
		}

		if (UserForgot != null && realCode != null && fakeCode != null) {
			if(fakeCode.equalsIgnoreCase(realCode)) {
				if(uc.edit(UserForgot)) {
					uc.releaseCon();
					request.getSession().removeAttribute("UserForgot");
					request.getSession().removeAttribute("code");
					request.getSession().invalidate();
					response.sendRedirect("/home/user/signin?successed");
				}else {
					uc.releaseCon();
					request.getSession().removeAttribute("UserForgot");
					request.getSession().removeAttribute("code");
					request.getSession().invalidate();
					response.sendRedirect("/home/user/signin?fall");
				}
			}else {
				response.sendRedirect("/home/user/verifier?sai-ma-xac-thuc");
			}
		} else {

			String username = request.getParameter("username");
			String newPass = request.getParameter("password");

			if (validate(username, newPass)) {
				UserObject item = uc.getByConditions(UserObject.class, "WHERE user_name = '" + username + "'");
				if (item != null) {
					String code = (new Random().nextInt(999999 - 100000) + 100000) + "";
					item.setUser_pass(newPass);
					VerificationEmail.sendVerificationCodes("daudaushopad@gmail.com", "Binhan296", item.getUser_email(),
							code);
					request.getSession().setAttribute("UserForgot", item);
					request.getSession().setAttribute("code", code);
					response.sendRedirect("/home/user/verifier");
				} else {
					response.sendRedirect("/home/user/forgot?tai-khoan-khong-ton-tai");
				}
			} else {
				response.sendRedirect("/home/user/forgot?error");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
