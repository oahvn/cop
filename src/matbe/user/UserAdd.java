package matbe.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.ED;
import com.oah.model.ConnectionPool;

import matbe.object.ClassObject;
import matbe.object.UserObject;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/user-add")
public class UserAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserAdd() {
		super();
		// TODO Auto-generated constructor stub
	}

	private HttpServletRequest req;

	private String get(String para) {
		return req.getParameter(para);
	}

	private boolean valide(String... paras) {
		for (String i : paras) {
			if (i == null || i.trim().equalsIgnoreCase(""))
				return false;
		}
		return true;
	}

	private int parseInt(String s) {
		return Integer.parseInt(s);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		this.req = request;
		UserObject UserLogined = (UserObject) request.getSession().getAttribute("UserLogined");
		String username = get("username");
		String password = get("password");
		String classname = get("classname");
		String teacher = get("teacher");
		String phone = get("phone");
		String address = get("address");
		String current = get("current");
		String start = get("start");
		String finish = get("finish");
		int class_user_id = UserLogined.getUser_id();

		if (valide(username, password, classname, teacher, address, phone, current, start, finish)) {
			int c = 0, s = 0, f = 0;
			try {
				c = parseInt(current);
				s = parseInt(start);
				f = parseInt(finish);
			} catch (NumberFormatException e) {

			}
			ClassObject item = new ClassObject();
			item.setClass_username(username);
			item.setClass_password(ED.encode(password));
			item.setClass_teacher_address(ED.encode(address));
			item.setClass_teacher_name(ED.encode(teacher));
			item.setClass_teacher_phone(phone);
			item.setClass_name(ED.encode(classname));
			item.setClass_current_year(c);
			item.setClass_finish_year(f);
			item.setClass_start_year(s);
			item.setClass_user_id(class_user_id);

			ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
			UserControl uc = new UserControl(cp);
			if (cp == null) {
				getServletContext().setAttribute("CP", uc.getCP());
			}

			ClassObject exist = uc.getByConditions(ClassObject.class, " WHERE class_username = '" + username + "'");
			uc.releaseCon();
			if (exist != null) {
				response.sendRedirect("/home/user/classes?TAI-KHOAN-DA-TON-TAI");
			} else {
				if (uc.add(item)) {
					uc.releaseCon();
					response.sendRedirect("/home/user/classes?pg=lastest&them-thanh-cong");
				}
			}

		} else {
			response.sendRedirect("/home/user/classes?fall");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
