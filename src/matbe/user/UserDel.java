package matbe.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.model.ConnectionPool;

import matbe.object.ClassObject;

/**
 * Servlet implementation class UserDel
 */
@WebServlet("/user-del")
public class UserDel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String page = request.getParameter("page");
		ConnectionPool cp  = (ConnectionPool) getServletContext().getAttribute("CP");
		UserControl uc = new UserControl(cp);
		if(cp == null) {
			getServletContext().setAttribute("CP", uc.getCP());
		}
		if(id!=null) {
			try {
				int ID = Integer.parseInt(id);
				ClassObject item = new ClassObject();
				item.setClass_id(ID);
				if(uc.delete(item)) {
					uc.releaseCon();
					response.sendRedirect("/home/user/classes?page="+page+"&delete-successed");
				}else {
					uc.releaseCon();
					response.sendRedirect("/home/user/classes?page="+page+"&xoa-that-bai");
				}
			}catch (NumberFormatException e) {
				response.sendRedirect("/home/user/classes?page="+page+"&xoa-that-bai");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
