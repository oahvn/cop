package matbe.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.ED;
import com.oah.model.ConnectionPool;

import matbe.admin.AdminControl;
import matbe.object.UserObject;

/**
 * Servlet implementation class UserChangePassword
 */
@WebServlet("/user-update-profile")
public class UserUpdateProFile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateProFile() {
		super();
		// TODO Auto-generated constructor stub
	}

	private boolean validate(String fullname, String mobilephone, String officephone, String address) {
		if (fullname == null || mobilephone == null || officephone == null || address == null ) {
			return false;
		} else {
			if (fullname.trim().equalsIgnoreCase("") || fullname.length() > 40) {
				System.out.println("Sai tên");
				return false;
			} else if (mobilephone.length() > 11) {
				System.out.println("Sai sđt");
				return false;
			} else if (officephone.length() > 11) {
				System.out.println("sai op");
				return false;

			}

			else if (address.trim().equalsIgnoreCase("") || address.length() > 200) {
				System.out.println("Sai address");
				return false;
			}

		}
		return true;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			request.setCharacterEncoding("utf-8");
			String fullname = request.getParameter("fullname");
			String mobilephone = request.getParameter("phone");
			String officephone = request.getParameter("officephone");
			String address = request.getParameter("address");
			ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
			AdminControl uc = new AdminControl(cp);
			if (cp == null) {
				getServletContext().setAttribute("CP", uc.getCP());
			}
			UserObject item = new UserObject();

			if (validate(fullname, mobilephone, officephone, address)) {
				item.setUser_id(id);
				item = uc.getById(item, "");
				item.setUser_fullname(ED.encode(fullname));
				item.setUser_mobilephone(mobilephone);
				item.setUser_officephone(officephone);
				item.setUser_address(ED.encode(address));
				if (uc.edit(item)) {
					uc.releaseCon();
					response.sendRedirect("/home/user/profiler?status=successed");
				} else {
					uc.releaseCon();
					response.sendRedirect("/home/user/profiler?status=fall1");
				}
			} else {
				response.sendRedirect("/home/user/profiler?status=fall2");
			}
		} catch (Exception e) {
			response.sendRedirect("/home/user/profiler?status=fall3");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
