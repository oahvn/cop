package matbe.user;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.ED;
import com.oah.model.ConnectionPool;

import matbe.admin.AdminControl;
import matbe.object.UserObject;

/**
 * Servlet implementation class UserSignup
 */
@WebServlet("/user-signup")
public class UserSignup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserSignup() {
		super();
		// TODO Auto-generated constructor stub
	}

	private boolean validate(String username, String email, String pass, String pass2, String fullname,
			String address) {
		if (username != null && email != null && pass != null && pass2 != null && fullname != null && address != null) {
			String regUser = "/[^a-z0-9A-Z]/";
			String regPass = "/[ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]/";
			String regVi = "/[^a-z0-9A-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]/";
			if (username.trim().indexOf(" ") > 0) {
				return false;
			} else if (username.trim().length() == 0) {
				return false;
			} else if (username.trim().length() > 30) {
				return false;
			} else if (username.trim().length() < 6) {
				return false;
			} else if (username.trim().matches(regUser)) {
				return false;
			} else if (pass.trim().indexOf(' ') > 0) {
				return false;
			} else if (pass.trim() == "") {
				return false;
			} else if (pass.trim().length() > 30) {
				return false;
			} else if (pass.trim().length() < 6) {
				return false;
			} else if (pass.trim().matches(regPass)) {
				return false;
			} else if (!pass2.equals(pass)) {
				return false;
			} else if (fullname.trim().length() == 0) {
				return false;
			} else if (fullname.trim().length() > 50) {
				return false;
			} else if (fullname.trim().matches(regVi)) {
				return false;
			} else if (address.trim().length() == 0) {
				return false;
			} else if (address.trim().matches(regVi)) {
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		UserObject UserSignup = (UserObject) request.getSession().getAttribute("UserSignup");
		String realCode = (String) request.getSession().getAttribute("code");
		String fakeCode = request.getParameter("code");
		if (UserSignup != null && fakeCode != null) {
			if (realCode.equalsIgnoreCase(fakeCode)) {
				ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
				AdminControl adc = new AdminControl(cp);
				if (cp == null) {
					getServletContext().setAttribute("CP", adc.getCP());
				}
				if (adc.add(UserSignup)) {
					request.getSession().removeAttribute("UserSignup");
					request.getSession().removeAttribute("code");
					adc.releaseCon();
					response.sendRedirect("/home/user/signin?successed");
				} else {
					request.getSession().removeAttribute("UserSignup");
					request.getSession().removeAttribute("code");
					adc.releaseCon();
					response.sendRedirect("/home/user/signin?fall");
				}
			} else {
				response.sendRedirect("/home/user/verifier");
			}
		} else {
			String username = request.getParameter("username");
			String pass = request.getParameter("password");
			String pass2 = request.getParameter("password2");
			String email = request.getParameter("email");
			String fullname = request.getParameter("fullname");
			String address = request.getParameter("address");
			if (validate(username, email, pass, pass2, fullname, address)) {
				ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
				AdminControl adc = new AdminControl(cp);
				if (cp == null) {
					getServletContext().setAttribute("CP", adc.getCP());
				}
				UserObject exist = new UserObject();
				exist = adc.getByConditions(UserObject.class, " WHERE user_name = '" + username + "'");
				adc.releaseCon();
				if (exist != null) {
					response.sendRedirect("/home/user/signup?err=tai-khoan-da-ton-tai");
				} else {
					String user_created_date = new SimpleDateFormat("yyyy-mm-dd").format(new Date());
					UserObject item = new UserObject();
					item.setUser_name(username);
					item.setUser_fullname(ED.encode(fullname));
					item.setUser_address(ED.encode(address));
					item.setUser_permission(2);
					item.setUser_pass(ED.encode(pass));
					item.setUser_created_date(user_created_date);
					item.setUser_email(email);
					String code = (new Random().nextInt(999999 - 100000) + 100000) + "";
					VerificationEmail.sendVerificationCodes("daudaushopad@gmail.com", "Binhan296", email, code);
					request.getSession().setAttribute("UserSignup", item);
					request.getSession().setAttribute("code", code);
					response.sendRedirect("/home/user/verifier");
				}
			}else {
				response.sendRedirect("/home/user/signup");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
