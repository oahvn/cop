package matbe.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.model.ConnectionPool;

import matbe.object.StudentObject;

/**
 * Servlet implementation class UserDS
 */
@WebServlet("/user-del-student")
public class UserDS extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDS() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
		UserControl uc = new UserControl(cp);
		if (cp == null) {
			getServletContext().setAttribute("CP", uc.getCP());
		}
		
		String id = request.getParameter("id");
		String cId = request.getParameter("cId");
		String page = request.getParameter("page");
		int ID = 0;
		if(id!=null) {
			try {
				ID = Integer.parseInt(id);
			}catch (NumberFormatException e) {
				// TODO: handle exception
			}
			StudentObject item = new StudentObject();
			item.setStudent_id(ID);
			if(uc.delete(item)) {
				uc.releaseCon();
				response.sendRedirect("/home/user/classes/students?id="+cId+"&page="+page);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
