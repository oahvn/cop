package matbe.user;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oah.ED;
import com.oah.model.ConnectionPool;

import matbe.object.ClassObject;

/**
 * Servlet implementation class UserEdit
 */
@WebServlet("/user-edit")
public class UserEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEdit() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String get(HttpServletRequest request, String parameter) {
		return request.getParameter(parameter);
	}

	private boolean validate(String... parameter) {
		int c = 0;
		for (String i : parameter) {

			if (i == null) {
				System.out.println(i + (++c));
				return false;
			}
		}
		return true;
	}

	private boolean validateLength(String para, int start, int end) {
		if (para.trim().equalsIgnoreCase("") || para.trim().length() > end || para.trim().length() < start)
			return false;
		return true;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String cname = request.getParameter("classname");
		String tname = request.getParameter("teacher");
		String taddress = request.getParameter("address");
		String tphone = request.getParameter("phone");
		String current = request.getParameter("current");
		String start = get(request, "start");
		String finish = get(request, "finish");
		String password = get(request, "password");
		String page = get(request, "page");
		System.out.println("ID: " + id);
		if (validate(id, cname, taddress, tname, tphone, current, start, finish, password)) {
			if (!id.equalsIgnoreCase("")) {
				ConnectionPool cp = (ConnectionPool) getServletContext().getAttribute("CP");
				UserControl uc = new UserControl(cp);
				ClassObject item = new ClassObject();
				if (cp == null) {
					getServletContext().setAttribute("CP", uc.getCP());
				}
				int ID = 0;
				if (id != null) {
					try {
						ID = Integer.parseInt(id);
					} catch (NumberFormatException e) {

					}
				}
				item.setClass_id(ID);
				item = uc.getById(item, "");
				uc.releaseCon();
				if (item != null) {
					int c = 0;
					int s = 0;
					int f = 0;
					try {
						c = Integer.parseInt(current);
						s = Integer.parseInt(start);
						f = Integer.parseInt(finish);
					} catch (NumberFormatException e) {

					}

					if (validateLength(cname, 1, 100) && validateLength(tname, 1, 40)
							&& validateLength(taddress, 1, 200) && validateLength(tphone, 1, 13)
							&& validateLength(password, 6, 30)) {
						item.setClass_current_year(c);
						item.setClass_finish_year(f);
						item.setClass_start_year(s);
						item.setClass_name(ED.encode(cname));
						item.setClass_teacher_address(ED.encode(taddress));
						item.setClass_teacher_name(ED.encode(tname));
						item.setClass_teacher_phone(tphone);
						item.setClass_password(ED.encode(password));
						if (uc.edit(item)) {
							uc.releaseCon();
							response.sendRedirect("/home/user/classes?successed&page=" + page);
						} else {
							uc.releaseCon();
							response.sendRedirect("/home/user/classes?cap-nhat-that-bai2&page=" + page);
						}
					}
				} else {
					response.sendRedirect("/home/user/classes?cap-nhat-that-bai3&page=" + page);
				}

			} else {
				response.sendRedirect("/home/user/classes?cap-nhat-that-bai4&page=" + page);
			}
		} else {
			response.sendRedirect("/home/user/classes?cap-nhat-that-bai5&page=" + page);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
