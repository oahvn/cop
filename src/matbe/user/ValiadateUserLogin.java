package matbe.user;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import matbe.object.UserObject;

/**
 * Servlet Filter implementation class ValiadateUserLogin
 */
@WebFilter(urlPatterns= {"/user/classes/*","/user/home","/user/classes","/user-edit/*","/user-del/*","/user-add/*",
		"/user/home/*","/user/change-password/*","/user/profiler/*"})
public class ValiadateUserLogin implements Filter {

	
	/**
	 * Default constructor.
	 */
	public ValiadateUserLogin() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		UserObject UserLogined = (UserObject) session.getAttribute("UserLogined");
		if (UserLogined != null && UserLogined.getUser_permission() == 2) {
			chain.doFilter(request, response);
		}else {
			res.sendRedirect("/home/user/signin");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
