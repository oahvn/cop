package matbe.object;import com.oah.annotations.Column;import com.oah.annotations.Entity;import com.oah.annotations.PK;import com.oah.annotations.Table;@Entity@Table(name="tblstudent")public class StudentObject{	@Column(name="student_id")	@PK
	 private int student_id;	@Column(name="student_name")
	 private String student_name;	@Column(name="student_ismale")
	 private String student_ismale;	@Column(name="student_birthday")
	 private String student_birthday;	@Column(name="student_address")
	 private String student_address;	@Column(name="student_class_id")
	 private int student_class_id;	public int getStudent_id() {
		return student_id;
	}	public String getStudent_name() {
		return student_name;
	}	public String getStudent_ismale() {
		return student_ismale;
	}	public String getStudent_birthday() {
		return student_birthday;
	}	public String getStudent_address() {
		return student_address;
	}	public int getStudent_class_id() {
		return student_class_id;
	}	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}	public void setStudent_ismale(String student_ismale) {
		this.student_ismale = student_ismale;
	}	public void setStudent_birthday(String student_birthday) {
		this.student_birthday = student_birthday;
	}	public void setStudent_address(String student_address) {
		this.student_address = student_address;
	}	public void setStudent_class_id(int student_class_id) {
		this.student_class_id = student_class_id;
	}}