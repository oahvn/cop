package matbe.object;
import com.oah.ED;
import com.oah.annotations.Column;
import com.oah.annotations.Entity;
import com.oah.annotations.PK;
import com.oah.annotations.Table;

@Entity
@Table(name = "tbldiop")
public class DiopObject extends StudentObject {
	@Column(name = "diop_id")
	@PK
	private int diop_id;
	
	@Column(name = "diop_student_id")
	private int diop_student_id;
	
	@Column(name = "diop_eye_left")
	private float diop_eye_left;
	
	@Column(name = "diop_eye_right")
	private float diop_eye_right;
	
	@Column(name = "diop_astigmatism_left")
	private float diop_astigmatism_left;
	
	@Column(name = "diop_astigmatism_right")
	private float diop_astigmatism_right;
	
	@Column(name = "diop_inputdate")
	private String diop_inputdate;
	
	@Column(name = "diop_status")
	private String diop_status;

	public int getDiop_id() {
		return diop_id;
	}

	public int getDiop_student_id() {
		return diop_student_id;
	}

	public float getDiop_eye_left() {
		return diop_eye_left;
	}

	public float getDiop_eye_right() {
		return diop_eye_right;
	}

	public float getDiop_astigmatism_left() {
		return diop_astigmatism_left;
	}

	public float getDiop_astigmatism_right() {
		return diop_astigmatism_right;
	}

	public String getDiop_inputdate() {
		return diop_inputdate;
	}

	public String getDiop_status() {
		return diop_status;
	}

	public void setDiop_id(int diop_id) {
		this.diop_id = diop_id;
	}

	public void setDiop_student_id(int diop_student_id) {
		this.diop_student_id = diop_student_id;
	}

	public void setDiop_eye_left(float diop_eye_left) {
		this.diop_eye_left = diop_eye_left;
	}

	public void setDiop_eye_right(float diop_eye_right) {
		this.diop_eye_right = diop_eye_right;
	}

	public void setDiop_astigmatism_left(float diop_astigmatism_left) {
		this.diop_astigmatism_left = diop_astigmatism_left;
	}

	public void setDiop_astigmatism_right(float diop_astigmatism_right) {
		this.diop_astigmatism_right = diop_astigmatism_right;
	}

	public void setDiop_inputdate(String diop_inputdate) {
		this.diop_inputdate = diop_inputdate;
	}

	public void setDiop_status(String diop_status) {
		this.diop_status = diop_status;
	}

}
