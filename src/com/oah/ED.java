package com.oah;

import net.htmlparser.jericho.*;

public class ED {
	public static String encode(String strUNI) {
		return CharacterReference.encode(strUNI);
	}

	public static String decode(String strHTML) {
		return CharacterReference.decode(strHTML);
	}
}
