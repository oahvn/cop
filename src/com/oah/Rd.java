package com.oah;

import java.time.LocalDate;
import java.util.Random;

public class Rd {

	public static String address() {
		Random rd = new Random();
		if (rd.nextBoolean()) {
			return IAddress1.address[rd.nextInt(IAddress1.END)];
		}
		return IAddress2.address[rd.nextInt(IAddress2.END)];
	}

	public static String name(boolean isMale) {
		Random rd = new Random();
		if (isMale) {
			return IName.hos[rd.nextInt(IName.hos.length)] + " " + IName.tensTrai[rd.nextInt(IName.tensTrai.length)];
		}
		return IName.hos[rd.nextInt(IName.hos.length)] + " " + IName.tensGai[rd.nextInt(IName.tensGai.length)];
	}

	public static String password() {
		Random rd = new Random();
		return (rd.nextInt(999999 - 100000) + 100000) + "";
	}
	
	public static String phone() {
		String[] head = { "086", "096", "097", "098", "032", "033", "034", "035", "036", "037", "038", "039" };
		Random rd = new Random();
		return head[rd.nextInt(head.length)] + (rd.nextInt(9999999 - 1000000) + 1000000);
	}

	public static String date(int start, int end) {
		Random random = new Random();
		int minDay = (int) LocalDate.of(start, 1, 1).toEpochDay();
		int maxDay = (int) LocalDate.of(end, 1, 1).toEpochDay();
		long randomDay = minDay + random.nextInt(maxDay - minDay);
		LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);
		return randomBirthDate.toString();
	}
}
