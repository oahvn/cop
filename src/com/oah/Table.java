package com.oah;

public class Table {
	public static String row(String...columns) {
		String tmp = "<tr>\n";
		for (String column : columns) {
			tmp += "\t<td>" + column + "</td>\n";
		}
		tmp += "</tr>\n";
		return tmp;
	}
	
	public static String header(String...ths) {
		String tmp ="<thead>\n";
		tmp+="<tr>\n";
		
		for(String th : ths) {
			tmp+= "\t<th>"+th+"</th>\n";
		}
		
		tmp+="</tr>\n";
		tmp+="</thead>\n";
		return tmp;
	}
}
