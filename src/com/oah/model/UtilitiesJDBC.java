package com.oah.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.beanutils.*;

import com.oah.annotations.Column;
import com.oah.annotations.FK;
import com.oah.annotations.PK;
import com.oah.annotations.Table;

public class UtilitiesJDBC {
	private Connection con;

	public UtilitiesJDBC(Connection con) {
		this.con = con;
	}

	private <T> boolean isTable(T item) {
		if (item != null) {
			if (item.getClass().isAnnotationPresent(Table.class)) {
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	private <T> Field[] getFields(T item) {
		if (isTable(item)) {
			return item.getClass().getDeclaredFields();
		}
		return null;
	}

	private boolean isPK(Field field) {
		if (field.isAnnotationPresent(PK.class))
			return true;
		return false;
	}

	private boolean isFK(Field field) {
		if (field.isAnnotationPresent(FK.class))
			return true;
		return false;
	}

	private boolean isColumn(Field field) {
		if (field.isAnnotationPresent(Column.class))
			return true;
		return false;
	}

	private String getName(Field field) {
		return field.getAnnotation(Column.class).name();
	}

	private <T> String getTableName(T item) {
		return item.getClass().getAnnotation(Table.class).name();
	}

	private <T> String insertSQL(T item) {
		if (isTable(item)) {
			StringBuilder sql = new StringBuilder("INSERT INTO ");
			sql.append(getTableName(item) + "(");
			String value = "VALUES(";
			Field[] fields = getFields(item);
			for (Field field : fields) {
				if (!isPK(field)) {
					sql.append(getName(field));
					sql.append(",");
					value += "?,";
				}
			}
			sql.replace(sql.length() - 1, sql.length(), ")");
			value = (value + ")").replace(",)", ")");
			sql.append(" " + value);
			return sql.toString();
		} else {
			throw new NullPointerException();
		}
	}

	private <T> String updateSQL(T item) {
		if (isTable(item)) {
			StringBuilder sql = new StringBuilder("UPDATE " + getTableName(item) + " SET ");
			String where = " WHERE ";
			Field[] fields = getFields(item);
			for (Field field : fields) {
				if (!isPK(field) && !isFK(field)) {
					if (isColumn(field)) {
						try {
							if (BeanUtils.getProperty(item, getName(field)) != null) {
								sql.append(getName(field) + " = ?, ");
							}
						} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
							e.printStackTrace();
						}
					}
				} else if (isFK(field)) {
					try {
						if (!BeanUtils.getProperty(item, getName(field)).equalsIgnoreCase("0")) {
							sql.append(getName(field) + " = ?, ");
						}
					} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
						e.printStackTrace();
					}
				} else {
					where += getName(field) + " = ?";
				}
			}
			sql.replace(sql.length() - 2, sql.length(), " ");
			sql.append(where);
			return sql.toString();
		} else {
			throw new NullPointerException();
		}
	}

	private <T> String deleteSQL(T item) {
		if (isTable(item)) {
			StringBuilder sql = new StringBuilder("DELETE FROM " + getTableName(item) + " WHERE ");
			Field[] fields = getFields(item);
			for (Field field : fields) {
				if (isPK(field)) {
					sql.append(getName(field) + " = ?");
				}
			}
			return sql.toString();
		} else {
			throw new NullPointerException();
		}
	}

	public <T> PreparedStatement setParamInsert(T item) {
		if (isTable(item)) {
			try {
				PreparedStatement preparedStatement = this.con.prepareStatement(insertSQL(item),
						PreparedStatement.RETURN_GENERATED_KEYS);
				Field[] fields = getFields(item);
				int i = 1;
				for (Field field : fields) {
					if (!isPK(field)) {
						preparedStatement.setObject(i++, BeanUtils.getProperty(item, getName(field)));
					}
				}
				System.out
						.println(preparedStatement.toString().substring(preparedStatement.toString().indexOf(":") + 2));
				return preparedStatement;
			} catch (SQLException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		throw new NullPointerException();
	}

	public <T> PreparedStatement setParamUpdate(T item) {
		if (isTable(item)) {
			PreparedStatement preparedStatement;
			try {
				preparedStatement = this.con.prepareStatement(updateSQL(item));
				Field[] fields = getFields(item);
				int i = 1;
				for (Field field : fields) {
					if (!isPK(field)) {
						if (isFK(field)) {
							if (!BeanUtils.getProperty(item, getName(field)).equalsIgnoreCase("0")) {
								preparedStatement.setObject(i++, BeanUtils.getProperty(item, getName(field)));
							}
						}
						if (isColumn(field) && !isFK(field)) {
							if (BeanUtils.getProperty(item, getName(field)) != null) {
								preparedStatement.setObject(i++, BeanUtils.getProperty(item, getName(field)));
							}
						}
					}
				}

				for (Field field : fields) {
					if (isPK(field)) {
						preparedStatement.setObject(i, BeanUtils.getProperty(item, getName(field)));
					}
				}
				System.out
						.println(preparedStatement.toString().substring(preparedStatement.toString().indexOf(":") + 2));
				return preparedStatement;
			} catch (SQLException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		throw new NullPointerException();
	}

	public <T> PreparedStatement setParamDelete(T item) {
		if (isTable(item)) {
			try {
				PreparedStatement preparedStatement;
				preparedStatement = this.con.prepareStatement(deleteSQL(item));
				Field[] fields = getFields(item);
				for (Field field : fields) {
					if (isPK(field)) {
						preparedStatement.setObject(1, BeanUtils.getProperty(item, getName(field)));
					}
				}
				System.out
						.println(preparedStatement.toString().substring(preparedStatement.toString().indexOf(":") + 2));
				return preparedStatement;
			} catch (SQLException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
				e.printStackTrace();
			}

		}
		throw new NullPointerException();
	}

	public <T> String gets(T Object, int at, int total, String conditions) {
		if (isTable(Object)) {
			StringBuilder sql = new StringBuilder("SELECT * FROM " + getTableName(Object));
			if (!conditions.equalsIgnoreCase("")) {
				sql.append(" " + conditions);
			}
			if (at >= 0) {
				sql.append(" LIMIT " + at + ", " + total);
			}
			System.out.println(sql.toString());
			return sql.toString();

		}
		throw new NullPointerException();
	}

	public <T> String getById(T item, String joinsString) {
		if (isTable(item)) {

			StringBuilder sql = new StringBuilder("SELECT * FROM " + getTableName(item)
					+ ((!joinsString.equalsIgnoreCase("")) ? " " + joinsString : "") + " WHERE ");
			Field[] fields = getFields(item);
			for (Field field : fields) {
				if (isPK(field)) {
					try {
						sql.append(getName(field) + " = " + BeanUtils.getProperty(item, getName(field)));
					} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
						e.printStackTrace();
					}
				}
			}
			System.out.println(sql.toString());
			return sql.toString();

		}
		throw new NullPointerException();
	}

	public <T> String getByConditions(T item, String conditions) {
		if (isTable(item)) {
			StringBuilder sb = new StringBuilder("SELECT * FROM " + getTableName(item) + " ");
			if (!conditions.equalsIgnoreCase("")) {
				sb.append(conditions);
			}
			System.out.println(sb.toString());
			return sb.toString();
		}
		throw new NullPointerException();
	}

}
