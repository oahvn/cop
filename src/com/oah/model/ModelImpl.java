package com.oah.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

class ModelImpl extends BasicImpl implements IModel {

	private UtilitiesJDBC utilities;

	public ModelImpl(ConnectionPool cp,String driver, String url, String username, String password) {
		super(cp, driver, url, username, password);
		utilities = new UtilitiesJDBC(this.con);
	}

	@Override
	public <T> boolean add(T item) {
		try {
			PreparedStatement preparedStatement = utilities.setParamInsert(item);
			return this.executeUpdate(preparedStatement);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public <T> ResultSet addRGK(T item) {
		PreparedStatement preparedStatement = utilities.setParamInsert(item);
		try {
			this.executeUpdate(preparedStatement);
			return preparedStatement.getGeneratedKeys();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

	@Override
	public <T> boolean edit(T item) {
		try {
			PreparedStatement preparedStatement = utilities.setParamUpdate(item);
			return this.executeUpdate(preparedStatement);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public <T> boolean delete(T item) {
		try {
			PreparedStatement preparedStatement = utilities.setParamDelete(item);
			return this.executeUpdate(preparedStatement);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public <T> ResultSet gets(T similar, int at, int total, String conditions) {
		String sql = this.utilities.gets(similar, at, total, conditions);
		return this.executeQuery(sql);
	}

	@Override
	public <T> ResultSet getById(T item, String joinsString) {
		String sql = this.utilities.getById(item, joinsString);
		return this.executeQuery(sql);
	}

	@Override
	public <T> ResultSet getByConditions(T item, String conditions) {
		String sql = this.utilities.getByConditions(item, conditions);
		return this.executeQuery(sql);
	}

}
