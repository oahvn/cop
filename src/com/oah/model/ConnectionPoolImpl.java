package com.oah.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Stack;

public class ConnectionPoolImpl implements ConnectionPool {
	private String url;
	private String username;
	private String password;
	private String driver;
	private Stack<Connection> pool;

	public ConnectionPoolImpl(String driver, String url, String username, String password) {
		this.driver = driver;
		loadDriver();
		this.url = url;
		this.username = username;
		this.password = password;
		this.pool = new Stack<Connection>();
	}

	private void loadDriver() {
		try {
			Class.forName(this.driver).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Connection getConnection() throws SQLException {
		if (this.pool.isEmpty()) {
			System.out.println("--*Created a connect*--");
			return DriverManager.getConnection(this.url, this.username, this.password);
		} else {
			System.out.println("--*Popped a connect*--");
			return pool.pop();
		}
	}

	@Override
	public void releaseConnection(Connection con) throws SQLException {
		System.out.println("--*Push a connect*--");
		this.pool.push(con);
	}

	public void finalize() throws Throwable {
		this.pool.clear();
		this.pool = null;
		System.gc();
		System.runFinalization();
		System.out.println("CPool is closed!");
	}
}
