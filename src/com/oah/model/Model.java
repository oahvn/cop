package com.oah.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Model {
	private IModel c;

	public Model(ConnectionPool cp, String driver, String url, String username, String password) {
		c = new ModelImpl(cp, driver, url, username, password);
	}

	public void releaseCon() {
		this.c.realeaseCon();
	}

	public ConnectionPool getCP() {
		return this.c.getConnectionPool();
	}

	public <T> boolean add(T item) {
		return this.c.add(item);
	}

	public <T> int addRGK(T item) {
		ResultSet resultSet = this.c.addRGK(item);
		if (resultSet != null) {
			try {
				if (resultSet.next()) {
					return resultSet.getInt(1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	public <T> boolean edit(T item) {
		return this.c.edit(item);
	}

	public <T> boolean delete(T item) {
		return this.c.delete(item);
	}

	public <T> T getById(T item, String joins) {
		ResultSet resultSet = this.c.getById(item, joins);
		return Mapper.mapObject(resultSet, item);
	}

	public <T> T getByConditions(Class<T> item, String conditions) {
		T i = null;
		try {
			i = item.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResultSet resultSet = this.c.getByConditions(i, conditions);
		return (T) Mapper.mapObject(resultSet, i);
	}

	public <T> ArrayList<T> getAll(Class<T> item,String conditions) {
		T i = null;
		try {
			i = item.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		ResultSet rs = this.c.getByConditions(i, conditions);
		return Mapper.mapObjects(rs, i);
	}

	public <T> ArrayList<T> gets(T similar, int page, int total, String conditions) {
		try {
			int at = (page - 1) * total;
			ResultSet resultSet = this.c.gets(similar, at, total, conditions);
			return Mapper.mapObjects(resultSet, similar);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
