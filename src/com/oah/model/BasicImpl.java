package com.oah.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BasicImpl implements IBasic {

	protected Connection con;
	private ConnectionPool cp;

	public BasicImpl(ConnectionPool cp, String driver, String url, String username, String password) {
		if (cp == null) {
			this.cp = new ConnectionPoolImpl(driver, url, username, password);
		} else {
			this.cp = cp;
		}

		try {
			this.con = this.cp.getConnection();
			if (this.con.getAutoCommit()) {
				this.con.setAutoCommit(false);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ConnectionPool getConnectionPool() {
		return this.cp;
	}

	@Override
	public void realeaseCon() {
		try {
			this.cp.releaseConnection(this.con);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean executeUpdate(PreparedStatement preparedStatement) {
		if (preparedStatement != null) {
			try {
				if (preparedStatement.executeUpdate() == 0) {
					this.con.rollback();
					return false;
				}
				this.con.commit();
				return true;
			} catch (SQLException e) {
				try {
					this.con.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public ResultSet executeQuery(String sql) {
		try {
			PreparedStatement preparedstatement = this.con.prepareStatement(sql);
			return preparedstatement.executeQuery(sql);
		} catch (SQLException e) {
			try {
				this.con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

		return null;
	}

}
