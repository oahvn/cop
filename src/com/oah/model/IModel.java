package com.oah.model;

import java.sql.ResultSet;

interface IModel extends IBasic{
	public <T> boolean add(T item);
	public <T> ResultSet addRGK(T item);
	public <T> boolean edit(T item);
	public <T> boolean delete(T item);
	public <T> ResultSet gets(T similar, int at, int total, String conditions);
	public <T> ResultSet getById(T item,String joinsString);
	public <T> ResultSet getByConditions(T item, String conditions);
}
