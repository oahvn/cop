package com.oah.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

import org.apache.commons.beanutils.BeanUtils;

class Mapper {
	public static <T> T mapObject(ResultSet resultSet, T item) {
		try {
			if (resultSet != null) {
				ResultSetMetaData rsmd = resultSet.getMetaData();
				if (resultSet.next()) {
					for (int i = 0; i < rsmd.getColumnCount(); i++) {
						String columnName = rsmd.getColumnName(i + 1);
						Object columnValue = resultSet.getObject(i + 1);
						BeanUtils.setProperty(item, columnName, columnValue);
					}
					return item;
				} else {
					return null;
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static <T> ArrayList<T> mapObjects(ResultSet resultSet, T similar) {
		if (resultSet != null) {
			try {
				ArrayList<T> items = new ArrayList<T>();
				T item = null;
				ResultSetMetaData rsmd = resultSet.getMetaData();
				while (resultSet.next()) {
					item = (T) similar.getClass().newInstance();
					for (int i = 0; i < rsmd.getColumnCount(); i++) {
						String columnName = rsmd.getColumnName(i + 1);
						Object columnValue = resultSet.getObject(i + 1);
						BeanUtils.setProperty(item, columnName, columnValue);
					}
					items.add(item);
				}
				return items;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;

	}

}
