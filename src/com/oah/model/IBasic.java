package com.oah.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

interface IBasic extends IShareControl {
	public boolean executeUpdate(PreparedStatement preparedStatement);
	public ResultSet executeQuery(String sql);
}
