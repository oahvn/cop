package com.oah.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

public class RenderDBtoOb {
	private String tableName;
	private String objectName;
	private BasicImpl basic;

	public RenderDBtoOb(String tableName, String objectName, String driver, String url, String username, String password) {
		this.basic = new BasicImpl(new ConnectionPoolImpl(driver, url, username, password), driver, url, username,
				password);
		this.tableName = tableName;
		this.objectName = objectName;
	}

	public void create(String pathPackage) {
		ResultSetMetaData rsmd = getRSMD();
		/**
		 * -7 BIT -6 TINYINT -5 BIGINT -4 LONGVARBINARY -3 VARBINARY -2 BINARY -1
		 * LONGVARCHAR 0 NULL 1 CHAR 2 NUMERIC 3 DECIMAL 4 INTEGER 5 SMALLINT 6 FLOAT 7
		 * REAL 8 DOUBLE 12 VARCHAR 91 DATE 92 TIME 93 TIMESTAMP 1111 OTHER
		 */

		/**
		 * -7 boolean -6 byte 1 12 92 93 91 String 4 int 6 float 8 double
		 * 
		 */
		ArrayList<Integer> types = new ArrayList<Integer>();
		ArrayList<String> fields = new ArrayList<String>();
		String pk = "";
		try {
			DatabaseMetaData dmd = this.basic.con.getMetaData();
			ResultSet rsPK = dmd.getPrimaryKeys("", "", this.tableName);
			if (rsPK.next()) {
				pk = rsPK.getString("COLUMN_NAME");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				types.add(rsmd.getColumnType(i));
				fields.add(rsmd.getColumnName(i));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String data = getPackage(pathPackage);
		data += "\rimport com.oah.annotations.Column;\r" + "import com.oah.annotations.Entity;\r"
				+ "import com.oah.annotations.PK;\r" + "import com.oah.annotations.Table;\r";
		data += "\r@Entity";
		data += "\r@Table(name=\"" + tableName + "\")\r";
		data += "public class " + this.objectName + "{\r";

		String getter = "";
		String setter = "";
		for (int i = 0; i < types.size(); i++) {
			int type = types.get(i);
			String field = fields.get(i);
			if (field.equalsIgnoreCase(pk)) {
				if (type == -7) {
					data += "	@Column(name=\"" + field + "\")\r\t@PK\n\t private boolean " + field + ";\r\r";
					getter += "	public boolean get" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "() {\r\n" + "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "(boolean " + field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n"
							+ "	}\r\r";
				}
				if (type == -6) {
					data += "	@Column(name=\"" + field + "\")\r\t@PK\n\t private byte " + field + ";\r\r";
					getter += "	public byte get" + field.substring(0, 1).toUpperCase() + field.substring(1) + "() {\r\n"
							+ "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(byte "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
				if (type == 1 || type == 12 || type == 91 || type == 92 || type == 93) {
					data += "	@Column(name=\"" + field + "\")\r\t@PK\n\t private String " + field + ";\r\r";
					getter += "	public String get" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "() {\r\n" + "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(String "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
				if (type == 2 || type == 3 || type == 4 || type == 5 || type == 7) {
					data += "	@Column(name=\"" + field + "\")\r\t@PK\n\t private int " + field + ";\r\r";
					getter += "	public int get" + field.substring(0, 1).toUpperCase() + field.substring(1) + "() {\r\n"
							+ "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(int "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
				if (type == 6) {
					data += "	@Column(name=\"" + field + "\")\r\t@PK\n\t private float " + field + ";\r\r";
					getter += "	public int float" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "() {\r\n" + "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(float "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
				if (type == 6) {
					data += "	@Column(name=\"" + field + "\")\r\t@PK\n\t private double " + field + ";\r\r";
					getter += "	public int double" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "() {\r\n" + "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(double "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
			} else {
				if (type == -7) {
					data += "	@Column(name=\"" + field + "\")\n\t private boolean " + field + ";\r\r";
					getter += "	public boolean get" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "() {\r\n" + "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "(boolean " + field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n"
							+ "	}\r\r";
				}
				if (type == -6) {
					data += "	@Column(name=\"" + field + "\")\n\t private byte " + field + ";\r\r";
					getter += "	public byte get" + field.substring(0, 1).toUpperCase() + field.substring(1) + "() {\r\n"
							+ "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(byte "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
				if (type == 1 || type == 12 || type == 91 || type == 92 || type == 93 || type == -1 || type == -4) {
					data += "	@Column(name=\"" + field + "\")\n\t private String " + field + ";\r\r";
					getter += "	public String get" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "() {\r\n" + "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(String "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
				if (type == 2 || type == 3 || type == 4 || type == 5) {
					data += "	@Column(name=\"" + field + "\")\n\t private int " + field + ";\r\r";
					getter += "	public int get" + field.substring(0, 1).toUpperCase() + field.substring(1) + "() {\r\n"
							+ "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(int "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
				if (type == 7) {
					data += "	@Column(name=\"" + field + "\")\n\t private float " + field + ";\r\r";
					getter += "	public int float" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "() {\r\n" + "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(float "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
				if (type == 8) {
					data += "	@Column(name=\"" + field + "\")\n\t private double " + field + ";\r\r";
					getter += "	public int double" + field.substring(0, 1).toUpperCase() + field.substring(1)
							+ "() {\r\n" + "		return " + field + ";\r\n" + "	}\r\r";
					setter += "	public void set" + field.substring(0, 1).toUpperCase() + field.substring(1) + "(double "
							+ field + ") {\r\n" + "		this." + field + " = " + field + ";\r\n" + "	}\r\r";
				}
			}
		}
		data += getter + setter;

		data += "}";
		File file = new File(pathPackage + "\\" + this.objectName + ".java");
		FileOutputStream fo = null;
		try {
			fo = new FileOutputStream(file);
			fo.write(data.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fo != null) {
				try {
					fo.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		this.basic.realeaseCon();
	}

	private String getPackage(String pathPackage) {
		String packageString = pathPackage.substring(pathPackage.indexOf("\\src"), pathPackage.length());
		return "package " + packageString.replace("\\src\\", "").replace("\\", ".") + ";";
	}

	private ResultSet getAll() {
		return this.basic.executeQuery("SELECT * FROM " + tableName);
	}

	private ResultSetMetaData getRSMD() {
		try {
			return this.getAll().getMetaData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
