<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="vi">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Đăng ký tài khoản</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
<link href="../../assets/vendor/fonts/circular-std/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="../../assets/libs/css/style.css">
<link rel="stylesheet"
	href="../../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
<style>
html, body {
	height: 100%;
}

* {
	font-family: arial;
}

body {
	display: -ms-flexbox;
	display: flex;
	-ms-flex-align: center;
	align-items: center;
	padding-top: 40px;
	padding-bottom: 40px;
}
</style>
</head>
<!-- ============================================================== -->
<!-- signup form  -->
<!-- ============================================================== -->

<body>
	<!-- ============================================================== -->
	<!-- signup form  -->
	<!-- ============================================================== -->
	<form id="frmSignup" method="POST" class="splash-container">
		<div class="card">
			<div class="card-header">
				<h3 class="mb-1">Đăng ký tài khoản</h3>
				<p>Hoàn tất thông tin để đăng ký tài khoản</p>
			</div>
			<div class="card-body">
				<div class="form-group">
					<input class="form-control form-control-lg" id="username"
						type="text" name="username" required="" placeholder="Tài khoản"
						autocomplete="off">
				</div>
				<div class="form-group">
					<input class="form-control form-control-lg" id="email" type="email"
						name="email" required placeholder="E-mail" autocomplete="off">
				</div>
				<div class="form-group">
					<input class="form-control form-control-lg" id="pass"
						type="password" name="password" required="" placeholder="Mật khẩu">
				</div>
				<div class="form-group">
					<input class="form-control form-control-lg" id="pass2"
						type="password" required="" name="password2"
						placeholder="Nhập lại mật khẩu">
				</div>
				<div class="form-group">
					<input class="form-control form-control-lg" id="fullname"
						type="text" required="" name="fullname"
						placeholder="Tên cá nhân, cơ quan...">
				</div>
				<div class="form-group">
					<input class="form-control form-control-lg" id="address"
						type="text" required="" name="address" placeholder="Địa chỉ">
				</div>
				<div class="form-group pt-2">
					<a href="javascript:submit()" class="btn btn-block btn-primary">Đăng
						ký</a>
				</div>
			</div>
			<div class="card-footer bg-white">
				<p>
					Đã có tài khoản? <a href="/home/user/signin" class="text-secondary">Đăng nhập.</a>
				</p>
			</div>
		</div>
	</form>
</body>
<script language="javascript">
	function validate() {
		var username = document.getElementById('username');
		var pass = document.getElementById('pass');
		var pass2 = document.getElementById('pass2');
		var fullname = document.getElementById('fullname');
		var address = document.getElementById('address');
		var email = document.getElementById('email');
		//_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ
		var regUser = /[^a-z0-9A-Z_]/;
		var regPass = /[ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]/;
		var regVi = /[^a-z0-9A-Z_\-\.ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]/;
		var regEmail = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
		if (username.value.trim().indexOf(' ') > 0) {
			alert('Tài khoản không chứa khoảng trống!');
			username.focus();
			username.select();
			return false;
		} else if (username.value.trim() == '') {
			alert('Tài khoản trống!');
			username.focus();
			username.select();
			return false;
		} else if (username.value.trim().length > 30) {
			alert('Tài khoản quá dài!');
			username.focus();
			username.select();
			return false;
		} else if (username.value.trim().length < 6) {
			alert('Tài khoản ngắn!');
			username.focus();
			username.select();
			return false;
		} else if (username.value.trim().match(regUser)) {
			alert('Tài khoản chứa ký tự đặc biệt!');
			username.focus();
			username.select();
			return false;
		}else if(!email.value.trim().match(regEmail)){
			alert('Sai thông tin email!');
			email.focus();
			email.select();
			return false;
		}else if (pass.value.trim().indexOf(' ') > 0) {
			alert('Mật khẩu không chứa khoảng trống!');
			pass.focus();
			pass.select();
			return false;
		} else if (pass.value.trim() == '') {
			alert('Mật khẩu trống!');
			pass.focus();
			pass.select();
			return false;
		} else if (pass.value.trim().length > 30) {
			alert('Mật khẩu quá dài!');
			pass.focus();
			pass.select();
			return false;
		} else if (pass.value.trim().length < 6) {
			alert('Mật khẩu quá ngắn!');
			pass.focus();
			pass.select();
			return false;
		} else if (pass.value.trim().match(regPass)) {
			alert('Mật khẩu chứa ký tự tiếng việt!');
			pass.focus();
			pass.select();
			return false;
		} else if (pass2.value !== pass.value) {
			alert('Mật khẩu không giống nhau!');
			pass2.focus();
			pass2.select();
			return false;
		} else if (fullname.value.trim().length == 0) {
			alert('Tên trống!');
			fullname.focus();
			fullname.select();
			return false;
		} else if (fullname.value.trim().length > 50) {
			alert('Tên quá dài!');
			fullname.focus();
			fullname.select();
			return false;
		} else if (fullname.value.trim().match(regVi)) {
			alert('Tên chứ ký tự đặc biệt!');
			fullname.focus();
			fullname.select();
			return false;
		} else if (address.value.trim().length == 0) {
			alert('Địa chỉ trống!');
			address.focus();
			address.select();
			return false;
		} else if (address.value.trim().match(regVi)) {
			alert('Địa chỉ chứa ký tự đặc biệt!');
			address.focus();
			address.select();
			return false;
		}

		return true;
	}

	function submit() {
		if (validate()) {
			document.getElementById("frmSignup").action = "/home/user-signup";
			document.getElementById("frmSignup").submit();
		}
		return false;
	}
</script>

</html>














