<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head lang="vi">
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Trang chủ</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
<link href="../../assets/vendor/fonts/circular-std/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="../../assets/libs/css/style.css">
<link rel="stylesheet"
	href="../../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
<style>
* {
	font-family: arial;
}
</style>
</head>

<body>
	<jsp:include page="../mn-hd.jsp"></jsp:include>
		<!-- ============================================================== -->
		<!-- wrapper  -->
		<!-- ============================================================== -->
		<div class="dashboard-wrapper">
			<div class="container-fluid dashboard-content">
				<!-- ============================================================== -->
				<!-- pageheader -->
				<!-- ============================================================== -->
				<div class="row">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="page-header">
							<h2>Home</h2>
							<p class="pageheader-text"></p>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end pageheader -->
				<!-- ============================================================== -->
				<div class="row">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<h3 class="text-center">Content goes here!</h3>
					</div>
				</div>
			</div>
			<jsp:include page="../footer.jsp"></jsp:include>
		</div>
		<!-- ============================================================== -->
		<!-- end main wrapper -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- end main wrapper -->
	<!-- ============================================================== -->
	<!-- Optional JavaScript -->
	<script src="../../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
	<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
	<script src="../../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
	<script src="../../assets/libs/js/main-js.js"></script>
</body>

</html>