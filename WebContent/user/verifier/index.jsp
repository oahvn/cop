<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="vi">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Đăng ký tài khoản</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../../assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/libs/css/style.css">
    <link rel="stylesheet" href="../../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
    html,
    body {
        height: 100%;
    }
	
	*{
		font-family: arial;
	}

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>
<!-- ============================================================== -->
<!-- signup form  -->
<!-- ============================================================== -->

<body>
	<%
		if(session.getAttribute("UserForgot")!=null){
	%>
<!-- ============================================================== -->
    <!-- signup form  -->
    <!-- ============================================================== -->
    <form id="validateCode" method="POST" class="splash-container">
        <div class="card">
            <div class="card-header">
                <h3 class="mb-1">Mã xác thực</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <input class="form-control form-control-lg" id="code" type="text" name="code" required="" placeholder="Mã xác thực" autocomplete="off">
                </div>
                <div class="form-group pt-2">
                    <a href="javascript:submit()" class="btn btn-block btn-primary">Xác thực</a>
                </div>
				 <div class="form-group pt-2">
                    <a href="/home/user/sigin" class="btn btn-block btn-primary">Hủy</a>
                </div>
            </div>
            <div class="card-footer bg-white">
                <p>Đã có tài khoản? <a href="/home/user/signin" class="text-secondary">Đăng nhập.</a></p>
            </div>
        </div>
    </form>
    <%}else{ %>


    <!-- ============================================================== -->
    <!-- signup form  -->
    <!-- ============================================================== -->
    <form id="validateCode" method="POST" class="splash-container">
        <div class="card">
            <div class="card-header">
                <h3 class="mb-1">Xác thực email</h3>
                <p>Hoàn tất thông tin để đăng ký tài khoản</p>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <input class="form-control form-control-lg" id="code" type="text" name="code" required="" placeholder="Mã xác thực" autocomplete="off">
                </div>
                <div class="form-group pt-2">
                    <a href="javascript:submit()" class="btn btn-block btn-primary">Xác thực</a>
                </div>
				 <div class="form-group pt-2">
                    <a href="/home/user/signin" class="btn btn-block btn-primary">Hủy</a>
                </div>
            </div>
            <div class="card-footer bg-white">
                <p>Đã có tài khoản? <a href="/home/user/signin" class="text-secondary">Đăng nhập.</a></p>
            </div>
        </div>
    </form>
    <%} %>
</body>
<script language="javascript">
	function validate(){
		var code = document.getElementById('code');
		if(code.value.trim().length != 6){
			alert('Sai mã xác thực!');
			code.focus();
			code.select();
			return false;
		}else if(code.value.match(/[^0-9]/)){
			alert('Sai mã xác thực!');
			code.focus();
			code.select();
			return false;
		}
		return true;
	}
	
	function submit(){
		if(validate()){
			<%if(session.getAttribute("UserForgot")!=null){%>
				document.getElementById("validateCode").action = "/home/user-forgot";
				document.getElementById("validateCode").submit();
			<%}else{%>
				document.getElementById("validateCode").action = "/home/user-signup";
				document.getElementById("validateCode").submit();
			<%}%>
		}
		return false;
	}
</script>
 
</html>