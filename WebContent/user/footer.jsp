<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<div class="footer fixed-bottom ">
	<div class="container-fluid">
		<div class="row	">
			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
				<div class="text-md-right footer-links d-none d-sm-block float-left">
					<a href="#" target="_blank">About</a> <a
						href="https://www.facebook.com/messages/t/notforgive.sunset" target="_blank">Support</a> <a
						href="https://www.facebook.com/messages/t/notforgive.sunset" target="_blank">Contact Us</a>
				</div>
			</div>
			<div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12 text-center">
				Copyright © <span id="year"></span> Concept. All rights reserved. Dashboard by <a
					href="https://www.facebook.com/notforgive.sunset">Hào</a>.
			</div>
		<script>document.getElementById('year').innerText = new Date().getYear()-100+2000;</script>
		</div>
	</div>
</div>
<!-- ============================================================== -->
<!-- end footer -->
<!-- ============================================================== -->