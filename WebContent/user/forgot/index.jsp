<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Đăng nhập</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
<link href="../../assets/vendor/fonts/circular-std/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="../../assets/libs/css/style.css">
<link rel="stylesheet"
	href="../../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
<style>
html, body {
	height: 100%;
}

* {
	font-family: arial;
}

body {
	display: -ms-flexbox;
	display: flex;
	-ms-flex-align: center;
	align-items: center;
	padding-top: 40px;
	padding-bottom: 40px;
}
</style>
</head>
<%
	session.invalidate();
%>
<body>
	<!-- ============================================================== -->
	<!-- login page  -->
	<!-- ============================================================== -->
	<div class="splash-container">
		<div class="card ">
			<div class="card-header text-center">
				<a href="signin.jsp"><img class="logo-img" src="logo.png"
					alt="logo"></a>
					<span class="splash-description">--Tầm nhìn Việt Nam--</span>
			</div>
			<div class="card-body">
				<form id="userLogin" method="POST" action="">
					<div class="form-group">
						<input class="form-control form-control-lg" name="username" id="username"
							type="text" placeholder="Tài khoản" autocomplete="off">
					</div>
					<div class="form-group">
						<input class="form-control form-control-lg" name="password" id="password"
							type="password" placeholder="Mật khẩu mới">
					</div>
					<a href="javascript: submit()" class="btn btn-primary btn-lg btn-block">Xác nhận</a>
				</form>
			</div>
			<div class="card-footer bg-white p-0  ">
				<div class="card-footer-item card-footer-item-bordered">
					<a href="../signup" class="footer-link">Tạo tài khoản</a>
				</div>
				<div class="card-footer-item card-footer-item-bordered">
					<a href="../signin" class="footer-link">Đăng nhập</a>
				</div>
			</div>
		</div>
	</div>
	<script language="javascript">
		function validate() {
			var username = document.getElementById('username');
			var pass = document.getElementById('password');
			var regUser = /[^a-z0-9A-Z_]/;
			var regPass = /[ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]/;
			if (username.value.trim().indexOf(' ') > 0) {
				alert('Tài khoản không chứa khoảng trống!');
				username.focus();
				username.select();
				return false;
			} else if (username.value.trim() == '') {
				alert('Tài khoản trống!');
				username.focus();
				username.select();
				return false;
			} else if (username.value.trim().length > 30) {
				alert('Tài khoản quá dài!');
				username.focus();
				username.select();
				return false;
			} else if (username.value.trim().length < 6) {
				alert('Tài khoản ngắn!');
				username.focus();
				username.select();
				return false;
			} else if (username.value.trim().match(regUser)) {
				alert('Tài khoản chứa ký tự đặc biệt!');
				username.focus();
				username.select();
				return false;
			} else if (pass.value.trim().indexOf(' ') > 0) {
				alert('Mật khẩu không chứa khoảng trống!');
				pass.focus();
				pass.select();
				return false;
			} else if (pass.value.trim() == '') {
				alert('Mật khẩu trống!');
				pass.focus();
				pass.select();
				return false;
			} else if (pass.value.trim().length > 30) {
				alert('Mật khẩu quá dài!');
				pass.focus();
				pass.select();
				return false;
			} else if (pass.value.trim().length < 6) {
				alert('Mật khẩu quá ngắn!');
				pass.focus();
				pass.select();
				return false;
			} else if (pass.value.trim().match(regPass)) {
				alert('Mật khẩu chứa ký tự tiếng việt!');
				pass.focus();
				pass.select();
				return false;
			}
			return true;
		}

		function submit() {
			if (validate()) {
				document.getElementById('userLogin').action = "/home/user-forgot";
				document.getElementById('userLogin').submit();
			}
		}
	</script>
	<!-- ============================================================== -->
	<!-- end login page  -->
	<!-- ============================================================== -->
	<!-- Optional JavaScript -->
	<script src="../../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
	<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
</body>
</html>