<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Đổi mật khẩu</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../../assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/libs/css/style.css">
    <link rel="stylesheet" href="../../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
</head>

<body>
   <jsp:include page="../mn-hd.jsp"></jsp:include>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper bg-light">
            <div class="container-fluid dashboard-content">
            
                <div class="row mt-5">	    
	                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mx-auto">
	                    <div class="card">
		                    <div class="card-title">
		                    	<h2 class="title">Đổi mật khẩu</h2>
		                    </div>
		                    <div class="card-body">
		                        <form action="/home/user-change-password" method="POST" id="formCP">
		                        	<input type="hidden" value="${UserLogined.getUser_id()}" name="id">
		                        	<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Mật khẩu cũ</span>
										</div>
										<input class="form-control"  type="password" name="oldpass" id="oldpass" value="" aria-describedby="inputGroup-sizing-sm">									
									</div>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Mật khẩu mới</span>
										</div>
										<input class="form-control" type="password"  name="newpass" id="newpass" value="" aria-describedby="inputGroup-sizing-sm">
									</div>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Nhập lại mật khẩu mới</span>
										</div>
										<input class="form-control" type="password"  name="renewpass" id="renewpass" value="" aria-describedby="inputGroup-sizing-sm">
									</div>
									<a id="btnCP" class="btn btn-outline-success float-right">Xác nhận</a>
		                        </form>
		                        <script>
		                        	document.getElementById("btnCP").onclick = function(){
		                        		var oldpass = document.getElementById("oldpass");
		                        		var newpass = document.getElementById("newpass");
		                        		var renewpass = document.getElementById("renewpass");
		                        		
		                        		if(oldpass.value != ${UserLogined.getUser_pass()}){
		                        			alert("Mật khẩu cũ không chính xác");
		                        			oldpass.focus();
		                        			oldpass.select();
		                        		}else if(newpass.value == ${UserLogined.getUser_pass()}){
		                        			alert("Mật khẩu mới trùng với mật khẩu cũ");
		                        			newpass.focus();
		                        			newpass.select();
		                        		}else if(newpass.value.length < 6){
		                        			alert("Mật khẩu quá ngắn");
		                        			newpass.focus();
		                        			newpass.select();
		                        		}else if(newpass.value.length > 30){
		                        			alert("Mật khẩu quá dài");
		                        			newpass.focus();
		                        			newpass.select();
		                        		}else if(renewpass.value != newpass.value){
		                        			alert("Mật khẩu không khớp");
		                        			renewpass.focus();
		                        			renewpass.select();
		                        		}else if(renewpass.value.length < 6){
		                        			alert("Mật khẩu quá ngắn");
		                        			renewpass.focus();
		                        			renewpass.select();
		                        		}else if(renewpass.value.length > 30){
		                        			alert("Mật khẩu quá dài");
		                        			renewpass.focus();
		                        			renewpass.select();
		                        		}else{
		                        			document.getElementById("formCP").submit();
		                        		}
		                        	};
		                        </script>
	                        </div>
	                    </div>
	                </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           <jsp:include page="../footer.jsp"></jsp:include>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="../../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="../../assets/libs/js/main-js.js"></script>
</body>
 
</html>