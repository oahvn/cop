<%@page import="matbe.user.UserControl"%>
<%@page import="com.oah.model.ConnectionPool"%>
<%@page import="matbe.object.UserObject"%>
<%
	ConnectionPool cp = (ConnectionPool) application.getAttribute("CP");
	UserControl uc = new UserControl(cp);
	if(cp==null){
		application.setAttribute("CP", uc.getCP());
	}
	UserObject UserLogined =(UserObject) session.getAttribute("UserLogined");
	if(UserLogined!=null){
		UserLogined = uc.getById(UserLogined, "");
		uc.releaseCon();
		session.setAttribute("UserLogined",UserLogined);
	}
%>