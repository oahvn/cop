<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
 <jsp:include page="data.jsp" flush="true"></jsp:include>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Thông tin tài khoản</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../../assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/libs/css/style.css">
    <link rel="stylesheet" href="../../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
</head>

<body>
   <jsp:include page="../mn-hd.jsp"></jsp:include>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper bg-light">
            <div class="container-fluid dashboard-content">
            
                <div class="row mt-5">	    
	                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 mx-auto">
	                    <div class="card">
		                    <div class="card-title">
		                    	<h2 class="title">Thông tin tài khoản</h2>
		                    </div>
		                    <div class="card-body">
		                        <form id="formUD" action="/home/user-update-profile" method="POST">
		                        	<input type="hidden" name="id" value="${UserLogined.getUser_id()}">
		                        	<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Tài khoản</span>
										</div>
										<input class="form-control" disabled type="text" name="username" id="username" value="${UserLogined.getUser_name()}" aria-describedby="inputGroup-sizing-sm">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Ngày tạo</span>
										</div>
										<input class="form-control" type="text" disabled name="createdDate" id="createdDate" value="${UserLogined.getUser_created_date()}" aria-describedby="inputGroup-sizing-sm">
									</div>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Tên trường</span>
										</div>
										<input class="form-control" type="text" name="fullname" id="fullname" value="${UserLogined.getUser_fullname() }" aria-describedby="inputGroup-sizing-sm">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Số điện thoại</span>
										</div>
										<input class="form-control" type="text" name="phone" id="phone" value="${UserLogined.getUser_mobilephone() }" aria-describedby="inputGroup-sizing-sm">	
									</div>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Email</span>
										</div>
										<input class="form-control form-control-lg" disabled id="email" type="email"
										name="email" required placeholder="E-mail" value="${UserLogined.getUser_email() }" autocomplete="off">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Số máy</span>
										</div>
										<input class="form-control" type="text" name="officephone" id="officephone" value="${UserLogined.getUser_officephone() }" aria-describedby="inputGroup-sizing-sm">
									</div>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="inputGroup-sizing-sm">Địa chỉ</span>
										</div>
										<input class="form-control" type="text" name="address" id="address" value="${UserLogined.getUser_address() }" aria-describedby="inputGroup-sizing-sm">
									</div>
									<a id="btnUD" class="btn btn-outline-success float-right">Cập nhật</a>
		                        </form>
		                        <script>
		                        	document.getElementById("btnUD").onclick = function(){
		                        		var fullname = document.getElementById("fullname");
		                        		var officephone = document.getElementById("officephone");
		                        		var phone = document.getElementById("phone");
		                        		var address = document.getElementById("address");
		                        		var regVi = /[^a-z0-9A-Z_[-]\.ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]/;
		                        		
		                        		if(fullname.value.trim().length == 0){
		                        			alert('Tên trống');
		                        			fullname.focus();
		                        			fullname.select();
		
		                        		}else if(fullname.value.trim().length > 100){
		                        			alert('Tên quá dài');
		                        			fullname.focus();
		                        			fullname.select();
		
		                        		}else if(fullname.value.match(regVi)){
		                        			alert('Tên chứa ký tự đặc biệt');
		                        			fullname.focus();
		                        			fullname.select();		
		                        		}else if(address.value.trim().length == 0){
		                        			alert('Địa chỉ trống');
		                        			address.focus();
		                        			address.select();
		
		                        		}else if(address.value.trim().length > 200){
		                        			alert('Địa chỉ quá dài');
		                        			address.focus();
		                        			address.select();
		
		                        		}else if(address.value.match(regVi)){
		                        			alert('Địa chỉ chứa ký tự đặc biệt');
		                        			address.focus();
		                        			address.select();		
		                        		}else if(officephone.value.trim().length == 0){
		                        			alert('Số máy trống');
		                        			officephone.focus();
		                        			officephone.select();
		
		                        		}else if(officephone.value.trim().length > 13){
		                        			alert('Số máy quá dài');
		                        			officephone.focus();
		                        			officephone.select();
		                        		}else if(phone.value.trim().length == 0){
		                        			alert('Số điện thoại trống');
		                        			phone.focus();
		                        			phone.select();
		                        		}else if(phone.value.trim().length > 13){
		                        			alert('Số điện thoại quá dài');
		                        			phone.focus();
		                        			phone.select();
		                        		}else{
		                        			document.getElementById("formUD").submit();
		                        		}
		                        	};
		                        
		                        </script>
	                        </div>
	                    </div>
	                </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           <jsp:include page="../footer.jsp"></jsp:include>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="../../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="../../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="../../assets/libs/js/main-js.js"></script>
</body>
 
</html>