<%@page import="matbe.object.ClassObject"%>
<%@page import="matbe.user.UserControl"%>
<%@page import="com.oah.ED"%>
<%@page import="java.lang.NumberFormatException"%>
<%@page import="com.oah.model.ConnectionPool"%>
<%@page import="com.oah.model.ConnectionPoolImpl"%>
<%@page import="matbe.object.UserObject"%>
<%@page import="matbe.admin.AdminControl"%>
<%
	ConnectionPool cp = (ConnectionPool) application.getAttribute("CP");
	UserControl uc = new UserControl(cp);
	if (cp == null) {
		application.setAttribute("CP", uc.getCP());
	}
	UserObject UserLogined = (UserObject) session.getAttribute("UserLogined");

	String p = request.getParameter("page");
	String s = request.getParameter("s");
	String f = request.getParameter("f");
	String y = request.getParameter("y");
	String lastest = request.getParameter("pg");
	Object search = request.getParameter("search");
	Object rm = request.getAttribute("rm");

	int pg = 1;
	int start = 0;
	int finish = 0;
	int year = 0;
	if (p != null) {
		try {
			pg = Integer.parseInt(p);
		} catch (NumberFormatException ex) {
			pg = 1;
		}
	}

	if (y != null) {
		try {
			year = Integer.parseInt(y);
			session.setAttribute("year", year);
			session.removeAttribute("start");
			session.removeAttribute("finish");
		} catch (NumberFormatException ex) {
			year = 0;
		}
	} else {
		Object yr = session.getAttribute("year");
		if (yr != null) {
			try {
				year = Integer.parseInt(yr.toString());
			} catch (NumberFormatException ex) {
				year = 0;
			}
		}
	}

	if (s != null && f != null) {
		try {
			start = Integer.parseInt(s);
			finish = Integer.parseInt(f);
			session.setAttribute("start", start);
			session.setAttribute("finish", finish);
			session.removeAttribute("year");
		} catch (NumberFormatException ex) {
			start = 0;
			finish = 0;
		}
	} else {
		Object st = session.getAttribute("start");
		Object fn = session.getAttribute("finish");
		if (st != null && fn != null) {
			try {
				start = Integer.parseInt(st.toString());
				finish = Integer.parseInt(fn.toString());
				session.setAttribute("start", start);
				session.setAttribute("finish", finish);
				session.removeAttribute("year");
			} catch (NumberFormatException ex) {
				start = 0;
				finish = 0;
			}
		}
	}

	if (s == null && p == null && f == null && y == null) {
		pg = 1;
		start = 0;
		finish = 0;
		year = 0;
		session.removeAttribute("start");
		session.removeAttribute("finish");
		session.removeAttribute("year");
	}

	if (lastest != null) {
		pg = 10000;
	}

	ClassObject item = new ClassObject();
	item.setClass_user_id(UserLogined.getUser_id());
	item.setClass_start_year(start);
	item.setClass_finish_year(finish);
	item.setClass_current_year(year);
	if (search == null) {
		search = session.getAttribute("search");
		if (search != null) {
			item.setClass_name(ED.encode(search.toString()));
			item.setClass_start_year(0);
			item.setClass_finish_year(0);
			item.setClass_current_year(0);
		}
	} else {
		session.setAttribute("search", search);
		item.setClass_name(ED.encode(search.toString()));
		item.setClass_start_year(0);
		item.setClass_finish_year(0);
		item.setClass_current_year(0);
		if (search.toString().equalsIgnoreCase("rm")) {
			session.removeAttribute("search");
			item.setClass_name("");
		}
	}
	String classes = uc.viewClasses(item, pg, 10);
	session.setAttribute("classes", classes);
	uc.releaseCon();
%>