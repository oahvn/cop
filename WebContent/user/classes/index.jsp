<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="data.jsp" flush="true"></jsp:include>
<html lang="vi">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Danh sách người dùng</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
<link href="../../assets/vendor/fonts/circular-std/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="../../assets/libs/css/style.css">
<link rel="stylesheet"
	href="../../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
<link rel="stylesheet" href="style.css">

<style>
* {
	font-family: arial;
}
</style>
</head>

<body>
	<jsp:include page="../mn-hd.jsp"></jsp:include>
	<!-- ============================================================== -->
	<!-- wrapper  -->
	<!-- ============================================================== -->
	<div class="dashboard-wrapper">
		<div class="container-fluid dashboard-content">
			<div class="row">
				<!-- ============================================================== -->
				<!-- basic table  -->
				<!-- ============================================================== -->
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="card">
						<div class="card-body">
							<div class="card-title">
								<ul id="loc">	
									<li class="nav-item mb-2">
										<button id="btnAdd" class="btn btn-sm btn-success">Thêm</button>
									</li>
									<li class="nav-item">
										<form action="/home/user/classes">
											<input class="form-control" name="search" value="${search}"
												style="width: 75%; float: left;" type="text"
												placeholder="Tìm kiếm..."> <a
												<%if (session.getAttribute("search") != null)
													out.print("style=\"background:red;color:white;\"");
												%>
												href="/home/user/classes?search=rm" class="btn btn-sm">&times;</a>
										</form>
									</li>
								</ul>
							</div>
							<div id="modal" class="modal">
								<div class="modal-content">
									<span id="close" class="close">&times;</span>
									<p>
									<form id="form" method="POST" action="/home/user-add">
										<div class="input-group input-group-sm mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Tài
													khoản</span>
											</div>
											<input type="text" class="form-control" name="username"
												id="username" aria-describedby="inputGroup-sizing-sm">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Mật
													khẩu</span>
											</div>
											<input class="form-control" type="password" name="password"
												id="password" value=""
												aria-describedby="inputGroup-sizing-sm">
										</div>
										<div class="input-group input-group-sm mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Tên
													lớp</span>
											</div>
											<input class="form-control" type="text" name=""
												classname"" id="classname" value=""
												aria-describedby="inputGroup-sizing-sm">

										</div>
										<!-- Giáo viên chủ nghiệm -->
										<div class="input-group input-group-sm mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Giáo
													viên chủ nhiệm</span>
											</div>
											<input class="form-control" type="text" name="teacher"
												id="teacher" value=""
												aria-describedby="inputGroup-sizing-sm">

										</div>
										<!-- Địa chỉ -->
										<div class="input-group input-group-sm mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Địa
													chỉ</span>
											</div>
											<input class="form-control" type="text" name="address"
												id="address" value=""
												aria-describedby="inputGroup-sizing-sm">
										</div>
										<div class="input-group input-group-sm mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Số
													điện thoại</span>
											</div>
											<input class="form-control" type="text" name="phone"
												id="phone" value="" aria-describedby="inputGroup-sizing-sm">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Năm
													học hiện tại</span>
											</div>
											<input class="form-control" type="text" name="current"
												id="current" value=""
												aria-describedby="inputGroup-sizing-sm">
										</div>
										<!-- Khóa học -->
										<div class="input-group input-group-sm mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Niên
													khóa: Từ</span>
											</div>
											<input class="form-control" type="text" name="start"
												id="start" value="" aria-describedby="inputGroup-sizing-sm">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Đến</span>
											</div>
											<input class="form-control" type="text" name="finish"
												id="finish" value="" aria-describedby="inputGroup-sizing-sm">
										</div>
										<a class="btn float-right btn-success mt-2" href="#"
											id="btnUD">Thêm</a>
									</form>
									<script>
							document.getElementById("btnUD").onclick = function(){
								var username=document.getElementById("username");
								var password=document.getElementById("password");
								var classname=document.getElementById("classname");
								var teacher=document.getElementById("teacher");
								var phone = document.getElementById("phone");
								var address = document.getElementById("address");
								var start = document.getElementById("start");
								var finish = document.getElementById("finish");
								var current = document.getElementById("current");
								var form = document.getElementById("form");
								var regVi = /[^a-z0-9A-Z_\-\.ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]/;
								var regUser = /[^a-z0-9A-Z\-\.]/;
								if(username.value.trim().match(regVi)){
									alert("Tài khoản chứa kí tự đặc biệt");
									username.focus();
									username.select();
								}else if(username.value.trim().length  == 0){
									alert("Tài khoản trống");
									username.focus();
									username.select();
								}else if(username.value.trim().length  > 30){
									alert("Tài khoản quá dài");
									username.focus();
									username.select();
								}else if(classname.value.trim().match(regVi)){
									alert("Tên lớp chứa kí tự đặc biệt");
									classname.focus();
									classname.select();
								}else if(classname.value.trim().length  == 0){
									alert("Tên lớp trống");
									classname.focus();
									classname.select();
								}else if(classname.value.trim().length  > 100){
									alert("Tên lớp quá dài");
									classname.focus();
									classname.select();
								}else if(teacher.value.trim().match(regVi)){
									alert("Tên giáo viên chứa ký tự đặc biệt");
									teacher.focus();
									teacher.select();
								}else if(teacher.value.trim().length == 0){
									alert("Tên giáo viên quá ngắn");
									teacher.focus();
									teacher.select();
								}else if(teacher.value.trim().length > 30){
									alert("Tên giáo viên quá dài");
									teacher.focus();
									teacher.select();
								}else if(start.value.trim().length == 0){
									alert("Năm bắt đầu trống");
									start.focus();
									start.select();
								}else if(start.value > (new Date().getFullYear()+10)){
									alert("Năm bắt đầu quá lớn");
									start.focus();
									start.select();
								}else if(finish.value.trim().length == 0){
									alert("Năm kết thúc đầu trống");
									finish.focus();
									finish.select();
								}else if(finish.value > (new Date().getFullYear()+10)){
									alert("Năm kết thúc đầu quá lớn");
									finish.focus();
									finish.select();
								}else if(current.value < start.value || current.value > finish.value){
									alert("Sai năm học hiện tại");
									current.focus();
									current.select();
								}else{
									window.location.href = "/home/user-add?username="+username.value+"&password="+password.value+"&teacher="+teacher.value+"&phone="+phone.value+"&address="+address.value+"&current="+current.value+"&start="+start.value+"&finish="+finish.value+"&classname="+classname.value;
								}
							};
							
							var modal = document.getElementById("modal");
							var btnShow = document.getElementById("btnAdd");
							var close = document.getElementById("close");
							btnShow.onclick = function(){
								modal.style.display = "block";
							}
							close.onclick = function(){
								modal.style.display = "none";
							}
							window.onclick = function(event){
								if(event.target == modal){
									modal.style.display = "none";
								}
							}
					</script>
									</p>
								</div>
							</div>
							<div class="table-responsive">${classes}</div>
							<script>
										document.getElementById("slYear").onchange = function(){
											window.location.href = document.getElementById("slYear").value;
										};
										document.getElementById("slTerm").onchange = function(){
											window.location.href = document.getElementById("slTerm").value;
										};
									
								</script>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end basic table  -->
				<!-- ============================================================== -->
			</div>
		</div>
		<jsp:include page="../footer.jsp"></jsp:include>
	</div>
	<!-- ============================================================== -->
	<!-- end main wrapper -->
	<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- end main wrapper -->
	<!-- ============================================================== -->
	<!-- Optional JavaScript -->
	<script src="../../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
	<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
	<script src="../../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
	<script src="../../assets/libs/js/main-js.js"></script>
</body>
</html>