<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="data.jsp" flush="true"></jsp:include>
<html lang="vi">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Danh sách người dùng</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="../../../assets/vendor/bootstrap/css/bootstrap.min.css">
<link href="../../../assets/vendor/fonts/circular-std/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="../../../assets/libs/css/style.css">
<link rel="stylesheet"
	href="../../../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
<link rel="stylesheet" href="../style.css">

<style>
* {
	font-family: arial;
}
</style>
</head>

<body>
	<jsp:include page="../../mn-hd.jsp"></jsp:include>
	<!-- ============================================================== -->
	<!-- wrapper  -->
	<!-- ============================================================== -->
	<div class="dashboard-wrapper">
		<div class="container-fluid dashboard-content">
			<div class="row">
				<!-- ============================================================== -->
				<!-- basic table  -->
				<!-- ============================================================== -->
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="card">
						<div class="card-body">
							<div class="card-title">
								<h2><strong>Danh sách học sinh</strong></h2>
								<ul id="loc">
									<li class="nav-item mb-2">
										<button id="btnAdd" class="btn btn-sm btn-success">Thêm</button>
										<a href="/home/user/classes" class="btn btn-sm btn-success">Quay lại</a>
									</li>
									<li class="nav-item">
										<form action="/home/user/classes/students/?id=<%=request.getParameter("id")%>">
											<input class="form-control" name="search" value="${search}"
												style="width: 75%; float: left;" type="text"
												placeholder="Tìm kiếm...">
											<input type="hidden" name="id" value="<%=request.getParameter("id")%>">
												 <a
												<%if (session.getAttribute("search") != null)
													out.print("style=\"background:red;color:white;\"");
												%>
												href="/home/user/classes/students?search=rm&id=<%=request.getParameter("id")%>" class="btn btn-sm">&times;</a>
										</form>
									</li>
								</ul>
							</div>
							<div id="modal" class="modal">
								<div class="modal-content">
									<span id="close" class="close">&times;</span>
									<p>
									<form id="form" method="POST" action="/home/user-add-student">
										<div class="input-group input-group-sm mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Họ và tên</span>
											</div>
											<input class="form-control" type="text" id="name" value="" aria-describedby="inputGroup-sizing-sm">
										</div>
										<div class="input-group input-group-sm mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Giới tính</span>
											</div>
											<div class="custom-control custom-radio custom-control-inline ml-2">
											  <input type="radio" id="male" checked name="sex" class="custom-control-input">
											  <label class="custom-control-label"for="male">Nam</label>
											</div>
											<div class="custom-control custom-radio custom-control-inline">
											  <input type="radio" id="female" name="sex" class="custom-control-input">
											  <label class="custom-control-label" for="female">Nữ</label>
											</div>
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Ngày sinh</span>
											</div>
											<input class="form-control" type="DATE" id="birthday" value=""
												aria-describedby="inputGroup-sizing-sm">
											
										</div>
										<div class="input-group input-group-sm mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="inputGroup-sizing-sm">Địa chỉ</span>
											</div>
											<input class="form-control" type="text" id="address" value=""
												aria-describedby="inputGroup-sizing-sm">	
											
										</div>
										<input type="hidden" value="<%=request.getParameter("id")%>" id="id">
										<a class="btn float-right btn-success mt-2" href="#"
											id="btnUD">Thêm</a>
									</form>
									<script>
							document.getElementById("btnUD").onclick = function(){
								var name=document.getElementById("name");
								var male=document.getElementById("male");
								var birthday=document.getElementById("birthday");
								var address=document.getElementById("address");
								var regVi = /[^a-z0-9A-Z_\-\.ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]/;
								if(name.value.trim().match(regVi)){
									alert("Tên chứa kí tự đặc biệt");
									name.focus();
									name.select();
								}else if(name.value.trim().length  == 0){
									alert("Tên trống");
									name.focus();
									name.select();
								}else if(name.value.trim().length  > 30){
									alert("Tên quá dài");
									name.focus();
									name.select();
								}else if(birthday.value.trim()==0){
									alert("Ngày sinh trống");
									birthday.focus();
									birthday.select();
								}else if(address.value.trim().match(regVi)){
									alert("Địa chỉ chứ ký tự đặc biệt");
									address.focus();
									address.select();
								}else if(address.value.trim().length > 100){
									alert("Địa chỉ quá dài");
									address.focus();
									address.select();
								}else if(address.value.trim().length == 0){
									alert("Địa chỉ trống");
									address.focus();
									address.select();
								}else{
									var sex = "Nữ";
									if(male.checked){
										sex = "Nam";
									}
									var id = document.getElementById("id").value;
									var lastPage = document.getElementById("lastPage").value;
									window.location.href = "/home/user-add-student?page="+lastPage+"&name="+name.value+"&sex="+sex+"&birthday="+birthday.value+"&address="+address.value+"&id="+id;
								}
							};
							
							var modal = document.getElementById("modal");
							var btnShow = document.getElementById("btnAdd");
							var close = document.getElementById("close");
							btnShow.onclick = function(){
								modal.style.display = "block";
							}
							close.onclick = function(){
								modal.style.display = "none";
							}
							window.onclick = function(event){
								if(event.target == modal){
									modal.style.display = "none";
								}
							}
					</script>
									</p>
								</div>
							</div>
							<div class="table-responsive">${viewStudents}</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end basic table  -->
				<!-- ============================================================== -->
			</div>
		</div>
		<jsp:include page="../../footer.jsp"></jsp:include>
	</div>
	<!-- ============================================================== -->
	<!-- end main wrapper -->
	<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- end main wrapper -->
	<!-- ============================================================== -->
	<!-- Optional JavaScript -->
	<script src="../../../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
	<script src="../../../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
	<script src="../../../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
	<script src="../../../assets/libs/js/main-js.js"></script>
</body>
</html>