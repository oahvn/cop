
<%@page import="matbe.object.StudentObject"%>
<%@page import="matbe.user.UserControl"%>
<%@page import="com.oah.model.ConnectionPool"%>

<%
	ConnectionPool cp = (ConnectionPool) application.getAttribute("CP");
	UserControl uc = new UserControl(cp);
	if (cp == null) {
		application.setAttribute("CP", uc.getCP());
	}

	String class_id = request.getParameter("id");
	String p = request.getParameter("page");
	Object search = request.getParameter("search");

	int pg = 1;
	if (p != null) {
		try {
			pg = Integer.parseInt(p);
		} catch (NumberFormatException ex) {

		}
	}
	int id = 0;
	if (class_id != null) {
		try {
			id = Integer.parseInt(class_id);
		} catch (NumberFormatException ex) {

		}
	}

	StudentObject similar = new StudentObject();
	if (search == null) {
		search = session.getAttribute("search");
		if (search != null) {
			similar.setStudent_name(search.toString());
		}
	} else {
		if (search.toString().equalsIgnoreCase("rm")) {
			similar.setStudent_name("");
			session.removeAttribute("search");
		} else {
			session.setAttribute("search", search.toString());
			similar.setStudent_name(search.toString());
		}
	}
	similar.setStudent_class_id(id);
	String viewStudents = uc.viewStudents(similar, pg, 15);
	uc.releaseCon();
	session.setAttribute("viewStudents", viewStudents);
%>