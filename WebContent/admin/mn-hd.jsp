<%@page import="com.oah.ED"%>
<%@page import="matbe.object.UserObject"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- ============================================================== -->
	<!-- main wrapper -->
	<!-- ============================================================== -->
	<div class="dashboard-main-wrapper">
		<!-- ============================================================== -->
		<!-- navbar -->
		<!-- ============================================================== -->
		<div class="dashboard-header ">
			<nav class="navbar navbar-expand-lg bg-white fixed-top"> <a
				class="navbar-brand" href="/home/admin/home/">Trang chủ</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto navbar-right-top">
					<li class="nav-item">
						<form action="/home/admin/users">
							<div id="custom-search" class="top-search-bar">
								<input class="form-control" name="search" value="${search}"
									style="width: 75%; float: left;" type="text"
									placeholder="Tìm kiếm..."> <a
									<%if (session.getAttribute("search") != null)
				out.print("style=\"background:red;color:white;\"");%>
									href="/home/admin/users?search=" class="btn btn-sm">&times;</a>
							</div>
						</form>
					</li>

					<li class="nav-item dropdown nav-user"><a
						class="nav-link nav-user-img" href="#"
						id="navbarDropdownMenuLink2" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"><img
							src="../../assets/images/avatar-1.jpg" alt=""
							class="user-avatar-md rounded-circle"></a>
						<div class="dropdown-menu dropdown-menu-right nav-user-dropdown"
							aria-labelledby="navbarDropdownMenuLink2">
							<div class="nav-user-info">
								<h5 class="mb-0 text-white nav-user-name">${AdminLogined.getUser_fullname()}</h5>
								<span class="status"></span><span class="ml-2">${AdminLogined.getUser_permission() }</span>
							</div>
							<a class="dropdown-item" href="/home/admin-signout"><i
								class="fas fa-power-off mr-2"></i>Đăng xuất</a>
						</div></li>
				</ul>
			</div>
			</nav>
		</div>
		<!-- ============================================================== -->
		<!-- end navbar -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- left sidebar -->
		<!-- ============================================================== -->
		<div class="nav-left-sidebar sidebar-dark">
			<div class="menu-list">
				<nav class="navbar navbar-expand-lg navbar-light"> <a
					class="d-xl-none d-lg-none" href="#">Quản lý người dùng</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarNav" aria-controls="navbarNav"
					aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav flex-column">
						<li class="nav-divider">Menu</li>
						<li class="nav-item "><a class="nav-link active" href="#"
							data-toggle="collapse" aria-expanded="false"
							data-target="#submenu-1" aria-controls="submenu-1"><i
								class="fa fa-fw fa-user-circle"></i>Quản lý người dùng<span
								class="badge badge-success">6</span></a>
							<div id="submenu-1" class="collapse submenu" style="">
								<ul class="nav flex-column">
									<li class="nav-item"><a class="nav-link"
										href="/home/admin/home">Trang chủ</a></li>
									<li class="nav-item"><a class="nav-link"
										href="/home/admin/users">Danh sách người dùng</a></li>

								</ul>
							</div></li>
					</ul>
				</div>
				</nav>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- end left sidebar -->
		<!-- ============================================================== -->