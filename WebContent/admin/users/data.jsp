<%@page import="com.oah.ED"%>
<%@page import="java.lang.NumberFormatException"%>
<%@page import="com.oah.model.ConnectionPool"%>
<%@page import="com.oah.model.ConnectionPoolImpl"%>
<%@page import="matbe.object.UserObject"%>
<%@page import="matbe.admin.AdminControl"%>
<%
	//Lấy bộ quản lý kết nối
	ConnectionPool cp = (ConnectionPool) application.getAttribute("CP");
	AdminControl uc = new AdminControl(cp);
	if (cp == null) {
		application.setAttribute("CP", uc.getCP());
	}
	
	//Tìm kiếm
	String pageString = request.getParameter("page");
	Object search = request.getParameter("search");
	if (search == null) {
		search = (Object) session.getAttribute("search");
	}
	String validSearch = "";
	boolean searching = false;
	if (search != null) {
		if (!search.toString().trim().equalsIgnoreCase("")) {
			String s = search.toString();
			validSearch += " WHERE user_permission < 4  AND (user_fullname LIKE '%" + ED.encode(s) + "%'";
			validSearch += " OR user_address LIKE '%" + ED.encode(s) + "%'";
			validSearch += " OR user_name LIKE '%" + ED.encode(s) + "%'";
			validSearch += " OR user_mobilephone LIKE '%" + ED.encode(s) + "%'";
			validSearch += " OR user_email LIKE '%" + ED.encode(s) + "%')";
			searching = true;
		} else {
			session.removeAttribute("search");
		}
	} else {
		session.removeAttribute("search");
	}
	int p = 1;
	if (pageString != null) {
		try {
			p = Integer.parseInt(pageString);
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		}
	} else {
		p = 1;
	}
	
	//Đẩy ra view
	UserObject item = new UserObject();
	if (searching) {
		String users = uc.viewUsers(item, p, 16, (validSearch));
		session.setAttribute("users", users);
		session.setAttribute("search", search.toString());
	} else {
		String users = uc.viewUsers(item, p, 16, "WHERE user_permission < 4");
		session.setAttribute("users", users);
		session.removeAttribute("search");
	}
	uc.releaseCon();
%>