<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="data.jsp" flush="true"></jsp:include>
<html lang="vi">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Danh sách người dùng</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="../../assets/vendor/bootstrap/css/bootstrap.min.css">
<link href="../../assets/vendor/fonts/circular-std/style.css"
	rel="stylesheet">
<link rel="stylesheet" href="../../assets/libs/css/style.css">
<link rel="stylesheet"
	href="../../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
<link rel="stylesheet" href="style.css">

<style>
* {
	font-family: arial;
}
</style>
</head>

<body>
	<jsp:include page="../mn-hd.jsp"></jsp:include>
		<!-- ============================================================== -->
		<!-- wrapper  -->
		<!-- ============================================================== -->
		<div class="dashboard-wrapper">
			<div class="container-fluid dashboard-content">
				<div class="row">
					<!-- ============================================================== -->
					<!-- basic table  -->
					<!-- ============================================================== -->
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="card">
							<div class="card-body">
								<div class="table-responsive">${users}</div>
							</div>
						</div>
					</div>
					<!-- ============================================================== -->
					<!-- end basic table  -->
					<!-- ============================================================== -->
				</div>
			</div>
			<jsp:include page="../footer.jsp"></jsp:include>
		</div>
		<!-- ============================================================== -->
		<!-- end main wrapper -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- end main wrapper -->
	<!-- ============================================================== -->
	<!-- Optional JavaScript -->
	<script src="../../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
	<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
	<script src="../../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
	<script src="../../assets/libs/js/main-js.js"></script>
</body>
</html>